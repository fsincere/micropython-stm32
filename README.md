## MicroPython-stm32

Exemples divers de mise en oeuvre d'une carte NUCLEO STM32L476RG programmée en MicroPython.

![stm32-nucleo-64-development-board-with-stm32l476rg-mcu](images/stm32-nucleo-64-development-board-with-stm32l476rg-mcu.jpg)



### Pourquoi la carte NUCLEO STM32L476RG ?

- car on peut la programmer en MicroPython
    + on reste dans l'écosystème Python
    + programmation objet
    + langage interprété, bien pratique pour exécuter du code à la volée (pas de phase de compilation)
- je m'en sers pour mon enseignement de physique appliquée : 
    + je n'ai pas besoin de Wifi/Bluetooth
    + la carte possède deux convertisseurs DAC : on peut donc programmer n'importe quelle forme de signaux
- on trouve sur le web tous les drivers de capteurs
- en définitive, c'est une sorte de super carte Arduino Uno pour un prix similaire

### Environnement de développement

Sous Linux ou Windows, Thonny est actuellement l'outil de développement idéal.  

Thonny permet d'accéder facilement à l'interpréteur MicroPython, ainsi qu'à la mémoire Flash de la carte.


### Matériel

La carte possède un connecteur compatible Arduino.

On peut donc y brancher un shield Grove et tous les capteurs associés.

### Liens utiles

[https://micropython.org](https://micropython.org/)  
[https://micropython.org/download/NUCLEO_L476RG](https://micropython.org/download/NUCLEO_L476RG/)  
[MicroPython documentation](https://docs.micropython.org/en/latest/)  
[MicroPython on GitHub](https://github.com/micropython/micropython)

[STM32python : MicroPython pour STM32](https://stm32python.gitlab.io/fr/docs/Micropython/)  

[CircuitPython (un clone de MicroPython) par Adafruit](https://circuitpython.org/)  

[MC Hobby (Dominique Meurisse)](https://wiki.mchobby.be/index.php?title=MicroPython-Accueil)



