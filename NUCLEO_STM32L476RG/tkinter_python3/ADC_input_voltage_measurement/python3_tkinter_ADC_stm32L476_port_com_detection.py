# Cpython3
# (C) Fabrice Sincère

# test OK sous linux et sous windows

# test OK avec carte nucleo STM32L476
# MicroPython v1.19.1 on 2023-02-11; NUCLEO-L476RG with STM32L476RG
# MicroPython v1.17 on 2021-10-30; NUCLEO-L476RG with STM32L476RG
# MicroPython v1.19.1 on 2022-06-18; NUCLEO-WB55 with STM32WB55RGV6
# MicroPython v1.19.1 on 2022-06-18; USBDongle-WB55 with STM32WB55CGU6

from tkinter import *
import serial.tools.list_ports  # module serial

try:
    import pyboard
    # biblio :
    # https://docs.micropython.org/en/latest/reference/pyboard.py.html
    # téléchargement :
    # https://github.com/micropython/micropython/blob/master/tools/pyboard.py
except ModuleNotFoundError as e:
    print(e)
    print("Il manque le module pyboard.py \
(à placer dans le répertoire courant).")
    exit(1)

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


def choix_port_manuel():
    "on choisit parmi la liste des ports détectés"
    ports_com = serial.tools.list_ports.comports()
    print("\nPorts COM détectés :")
    if len(ports_com) == 0:
        print("pas de port COM détecté")
        exit(2)
    list_ports = []
    for i, port in enumerate(ports_com):
        print(i, "-->", port.device, port.description)
        list_ports.append(port.device)
    choix = int(input("Votre choix ? "))
    return list_ports[choix]


def detection_port_auto():
    """détection automatique du port où est connecté une carte STM32
ça marche pas à tous les coups !"""

    ports_com = serial.tools.list_ports.comports()
    port_detect = None
    print("\nPorts COM détectés :")
    if len(ports_com) == 0:
        print("pas de port COM détecté")
        exit(1)
    mots_cles = ['stm32', 'pyboard', 'stlink']
    for port in ports_com:
        print(port.device, port.description)
        for cle in mots_cles:
            if cle in port.description.lower():
                port_detect = port.device
                break
    if port_detect is None:
        # on tente avec le dernier port de la liste...
        port_detect = port.device
    return port_detect

    # sous linux, suivant les cartes, la description est la suivante :
    # Dongle stm32wb55 / stm32wb55 board :
    # "Pyboard Virtual Comm Port in FS Mode"
    # carte stm32L476 / B-L475E-IOT
    # "STM32 STLink - ST-Link VCP Ctrl"

    # sous windows :
    # Dongle stm32wb55 / stm32wb55 board
    # "Périphérique série USB"
    # carte stm32L476
    # "STMicroelectronics STLink Virtual COM Port"


print("Commencer par connecter la carte Nucleo")
input("Touche Enter si c'est fait")

choix = input("Détection auto du port COM ? (o/n)")
if choix == "o":
    port_detect = detection_port_auto()
else:
    port_detect = choix_port_manuel()

print("\nConnexion au port COM : {}".format(port_detect))

# connexion carte STM32
pyb = pyboard.Pyboard(port_detect, 115200)
pyb.enter_raw_repl()
pyb.exec('''from pyb import ADC
adc = pyb.ADC('A0')''')
print("[OK]")
print("""Lecture de la tension sur l'entrée A0 de la carte""")


def update():
    # on arrive ici toutes les 100 ms
    global ID
    res = int(pyb.exec('print(adc.read())'))
    val.set("{:.3f}".format(res*3.3/4095))
    ID = Mafenetre.after(100, update)


def quit():
    pyb.exit_raw_repl()
    # nécessaire sous Windows pour éviter un Warning...
    Mafenetre.after_cancel(ID)
    Mafenetre.destroy()


# Création de la fenêtre principale (main window)
Mafenetre = Tk()
Mafenetre.title("STM32L476 nucleo board")

val = StringVar()  # 0 à 4095
val.set(0)

# Création d'un widget Label
Label1 = Label(Mafenetre, text='Input pin A0 Voltage [volts] :')
# Positionnement du widget avec la méthode pack()
Label1.pack()


# Création d'un widget Label (affichage tension)
Label2 = Label(Mafenetre, textvariable=val, fg='red')
# Positionnement du widget avec la méthode pack()
Label2.pack()

# Création d'un widget Button (bouton Quitter)
Button(Mafenetre, text="Quit", command=quit).pack(padx=10, pady=10)

update()

Mafenetre.mainloop()
