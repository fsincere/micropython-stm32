# Cpython3
# (C) Fabrice Sincère

# test OK sous linux et sous windows

# test OK avec carte nucleo STM32L476
# MicroPython v1.19.1 on 2023-02-11; NUCLEO-L476RG with STM32L476RG
# MicroPython v1.17 on 2021-10-30; NUCLEO-L476RG with STM32L476RG
# MicroPython v1.19.1 on 2022-06-18; NUCLEO-WB55 with STM32WB55RGV6
# MicroPython v1.19.1 on 2022-06-18; USBDongle-WB55 with STM32WB55CGU6

from tkinter import *
try:
    import pyboard
    # biblio :
    # https://docs.micropython.org/en/latest/reference/pyboard.py.html
    # téléchargement :
    # https://github.com/micropython/micropython/blob/master/tools/pyboard.py
except ModuleNotFoundError as e:
    print(e)
    print("Il manque le module pyboard.py \
(à placer dans le répertoire courant).")
    exit(1)

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


print("""Lecture de la tension sur l'entrée A0 de la carte""")

# connexion carte STM32
pyb = pyboard.Pyboard('/dev/ttyACM0', 115200)  # port COM par défaut sous Linux
# pyb = pyboard.Pyboard('COM1', 115200)  # sous windows
pyb.enter_raw_repl()
pyb.exec('''from pyb import ADC
adc = pyb.ADC('A0')''')


def update():
    # on arrive ici toutes les 100 ms
    res = int(pyb.exec('print(adc.read())'))
    val.set("{:.3f}".format(res*3.3/4095))
    Mafenetre.after(100, update)


def quit():
    pyb.exit_raw_repl()
    Mafenetre.destroy()


# Création de la fenêtre principale (main window)
Mafenetre = Tk()
Mafenetre.title("STM32L476 nucleo board")

val = StringVar()  # 0 à 4095
val.set(0)

# Création d'un widget Label
Label1 = Label(Mafenetre, text='Input pin A0 Voltage [volts] :')
# Positionnement du widget avec la méthode pack()
Label1.pack()


# Création d'un widget Label (affichage tension)
Label2 = Label(Mafenetre, textvariable=val, fg='red')
# Positionnement du widget avec la méthode pack()
Label2.pack()

# Création d'un widget Button (bouton Quitter)
Button(Mafenetre, text="Quit", command=quit).pack(padx=10, pady=10)

update()

Mafenetre.mainloop()
