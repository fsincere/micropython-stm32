## Carte NUCLEO STM32L476RG

## Lecture en temps réel de la tension d'entrée (broche A0 de la carte) depuis une application graphique sur PC


Le script Python *python3_tkinter_ADC_stm32L476.py* est à lancer depuis un pc sous windows ou linux, une raspberry pi...  
N'oubliez pas de placer le module *pyboard.py* dans le répertoire courant.

Dans le code, il faut préciser manuellement le port com virtuel utilisé avec le câble de liaison USB.

![python3_tkinter_ADC_stm32L476.png](images/python3_tkinter_ADC_stm32L476.png)

Le script Python *python3_tkinter_ADC_stm32L476_port_com_detection.py* propose une détection automatique des ports :

```
Commencer par connecter la carte Nucleo
Touche Enter si c'est fait
Détection auto du port COM ? (o/n)

Ports COM détectés :
0 --> /dev/ttyS0 ttyS0
1 --> /dev/ttyACM0 Pyboard Virtual Comm Port in FS Mode
2 --> /dev/ttyACM1 STM32 STLink - ST-Link VCP Ctrl
Votre choix ? 2

Connexion au port COM : /dev/ttyACM1
[OK]
Lecture de la tension sur l'entrée A0 de la carte
```


Test OK avec :

- MicroPython v1.19.1 on 2023-02-11; NUCLEO-L476RG with STM32L476RG
- MicroPython v1.17 on 2021-10-30; NUCLEO-L476RG with STM32L476RG
- MicroPython v1.19.1 on 2022-06-18; NUCLEO-WB55 with STM32WB55RGV6
- MicroPython v1.19.1 on 2022-06-18; USBDongle-WB55 with STM32WB55CGU6

(C) Fabrice Sincère