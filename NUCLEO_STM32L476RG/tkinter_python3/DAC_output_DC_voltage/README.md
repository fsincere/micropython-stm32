## Carte NUCLEO STM32L476RG

## Commande de la sortie DAC de la carte depuis une application graphique sur PC


Le script Python *python3_tkinter_DAC_control_stm32L476.py* est à lancer depuis un pc sous windows ou linux, une raspberry pi...  
N'oubliez pas de placer le module *pyboard.py* dans le répertoire courant.

Dans le code, il faut préciser la sortie utilisée par la carte (A2 ou D13), ainsi que le port com virtuel utilisé avec le câble de liaison USB.

![python3_tkinter_DAC_control_stm32L476.png](images/python3_tkinter_DAC_control_stm32L476.png)

Plage de tension : 0 à 3.3 V

Attention :

- non linéarité des convertisseurs dans la plage 0 à 50 mV (défaut du circuit buffer ?)
- avec la sortie D13, saturation haute à 3,1 V (influence de la LED built-in ?)

Test OK avec :

- MicroPython v1.19.1 on 2023-02-11; NUCLEO-L476RG with STM32L476RG
- MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

(C) Fabrice Sincère