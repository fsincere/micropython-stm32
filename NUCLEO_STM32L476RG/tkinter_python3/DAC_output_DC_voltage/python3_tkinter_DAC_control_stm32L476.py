# Cpython3
# (C) Fabrice Sincère

# test OK sous linux et sous windows

# test OK avec carte nucleo STM32L476
# MicroPython v1.19.1 on 2023-02-11; NUCLEO-L476RG with STM32L476RG
# MicroPython v1.17 on 2021-10-30; NUCLEO-L476RG with STM32L476RG

# attention : avec sortie A2, saturation basse à 0,05 V (défaut du buffer)
# attention : avec sortie D13, saturation haute à 3,1 V et basse à 0,05 V
# (influence de la LED built-in)


from tkinter import *
try:
    import pyboard
    # biblio :
    # https://docs.micropython.org/en/latest/reference/pyboard.py.html
    # téléchargement :
    # https://github.com/micropython/micropython/blob/master/tools/pyboard.py
except ModuleNotFoundError as e:
    print(e)
    print("Il manque le module pyboard.py \
(à placer dans le répertoire courant).")
    exit(1)

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


pinout = 'D13'
# pinout = 'A2'

step1 = 0.01  # pas en volt
step2 = 0.1   # pas en volt


def update(val):
    pyb.exec('dac.write({})'.format(int(float(value.get())*4095/3.3)))


def plus():
    value.set(str(float(value.get())+step2))
    pyb.exec('dac.write({})'.format(int(float(value.get())*4095/3.3)))


def moins():
    value.set(str(float(value.get())-step2))
    pyb.exec('dac.write({})'.format(int(float(value.get())*4095/3.3)))


def quit():
    pyb.exec('dac.deinit()')
    pyb.exit_raw_repl()
    Mafenetre.destroy()


# connexion carte STM32L476
pyb = pyboard.Pyboard('/dev/ttyACM0', 115200)  # port COM par défaut sous Linux
# pyb = pyboard.Pyboard('COM1', 115200)  # sous windows
pyb.enter_raw_repl()
pyb.exec('from pyb import DAC')
if pinout == 'D13':
    pyb.exec('dac = DAC(2, bits=12, buffering=True)')  # sortie broche D13
else:
    pyb.exec('dac = DAC(1, bits=12, buffering=True)')  # sortie broche A2
pyb.exec('dac.write(0)')  # 0 volt

# Création de la fenêtre principale (main window)
Mafenetre = Tk()
Mafenetre.title("STM32L476 nucleo board")

value = StringVar()  # 0 à 3.3
value.set(0)
# Création d'un widget Scale
echelle = Scale(Mafenetre, from_=0, to=3.3, resolution=step1,
                orient=HORIZONTAL, length=500, width=20,
                label="DAC output (pin "+pinout+") [volts] :",
                tickinterval=0.5, variable=value, command=update)
echelle.pack(padx=10, pady=10)

# Création d'un widget Button (bouton +)
Button(Mafenetre, text="+", command=plus).pack(padx=10, pady=10)

# Création d'un widget Button (bouton -)
Button(Mafenetre, text="-", command=moins).pack(padx=10, pady=10)

# Création d'un widget Button (bouton Quitter)
Button(Mafenetre, text="Quit", command=quit).pack(padx=10, pady=10)

Mafenetre.mainloop()
