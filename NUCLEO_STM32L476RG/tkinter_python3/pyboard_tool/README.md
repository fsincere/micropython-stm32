The pyboard.py tool
===================

### Documentation

[https://docs.micropython.org/en/latest/reference/pyboard.py.html](https://docs.micropython.org/en/latest/reference/pyboard.py.html)

### Téléchargement

[https://github.com/micropython/micropython/blob/master/tools/pyboard.py](https://github.com/micropython/micropython/blob/master/tools/pyboard.py)


### Copyright

This file is part of the MicroPython project, [http://micropython.org/](http://micropython.org/)

The MIT License (MIT)  

Copyright (c) 2014-2019 Damien P. George  

Copyright (c) 2017 Paul Sokolovsky  

