# Cpython3
# (C) Fabrice Sincère

# test OK sous linux et sous windows

# test OK avec carte nucleo STM32L476
# MicroPython v1.19.1 on 2023-02-11; NUCLEO-L476RG with STM32L476RG
# MicroPython v1.17 on 2021-10-30; NUCLEO-L476RG with STM32L476RG


from tkinter import *
try:
    import pyboard
    # biblio :
    # https://docs.micropython.org/en/latest/reference/pyboard.py.html
    # téléchargement :
    # https://github.com/micropython/micropython/blob/master/tools/pyboard.py
except ModuleNotFoundError as e:
    print(e)
    print("Il manque le module pyboard.py \
(à placer dans le répertoire courant).")
    exit(1)

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


def toggle():
    pyb.exec('led.toggle()')


def on():
    pyb.exec('led.on()')


def off():
    pyb.exec('led.off()')


def quit():
    pyb.exec('led.off()')
    pyb.exit_raw_repl()
    Mafenetre.destroy()


# connexion carte STM32L476
pyb = pyboard.Pyboard('/dev/ttyACM0', 115200)  # port COM par défaut sous Linux
# pyb = pyboard.Pyboard('COM1', 115200)  # sous windows
pyb.enter_raw_repl()
pyb.exec('from pyb import LED')
pyb.exec('led = LED(1)')  # led built-in (pin D13)
pyb.exec('led.on()')

# Création de la fenêtre principale (main window)
Mafenetre = Tk()
Mafenetre.title("STM32L476 nucleo board")

# Création d'un widget Button (bouton on)
Button(Mafenetre, text="On", command=on).pack(padx=10, pady=10)

# Création d'un widget Button (bouton off)
Button(Mafenetre, text="Off", command=off).pack(padx=10, pady=10)

# Création d'un widget Button (bouton toggle)
Button(Mafenetre, text="Toggle", command=toggle).pack(padx=10, pady=10)

# Création d'un widget Button (bouton Quitter)
Button(Mafenetre, text="Quit", command=quit).pack(padx=10, pady=10)

Mafenetre.mainloop()
