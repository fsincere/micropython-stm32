## Carte NUCLEO STM32L476RG

## Commande de la LED built-in de la carte depuis une application graphique sur PC


Le script Python *python3_tkinter_LED_control_stm32L476.py* est à lancer depuis un pc sous windows ou linux, une raspberry pi...  
N'oubliez pas de placer le module *pyboard.py* dans le répertoire courant.

Dans le code, il faut préciser le port com virtuel utilisé avec le câble de liaison USB.

Cette carte possède une LED built-in en sortie D13 (LED verte "LD2" avec résistance série de 510 ohms).

![python3_tkinter_LED_control_stm32L476.png](images/python3_tkinter_LED_control_stm32L476.png)


Test OK avec :

- MicroPython v1.19.1 on 2023-02-11; NUCLEO-L476RG with STM32L476RG
- MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

(C) Fabrice Sincère