# (C) Fabrice Sincère
# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

"""
Bouton-poussoir externe, normalement ouvert,
à relier entre la broche D8 et la masse avec un condensateur en parallèle
de # 100 nF (anti-rebonds, indispensable)

Appuyer sur le bouton-poussoir user (bleu)
ou le bouton-poussoir externe
pour incrémenter les compteurs associés
"""

from pyb import Pin, ExtInt, Switch
import micropython

__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

micropython.alloc_emergency_exception_buf(100)


def interrupt_bp(event):
    #  interruption sur le bouton-poussoir externe
    global count
    if bp() == 0:
        # on appuie sur le bouton-poussoir
        count += 1
        print(count, "D8 switch ")


def interrupt_bp_user():
    #  interruption sur le bouton-poussoir user
    global count_user
    count_user += 1
    print(count_user, "user switch")


# variables globales
count, count_user = 0, 0

# bouton-poussoir USER
# bp au repos -> niveau 1
# bp appuyé   -> niveau 0
bp_user = Switch()
bp_user.callback(interrupt_bp_user)

# bouton-poussoir externe, normalement ouvert,
# à relier à la masse avec un condensateur en parallèle de # 100 nF
# (anti-rebonds)
# bp au repos -> niveau 1
# bp appuyé   -> niveau 0
bp = Pin("D8", mode=Pin.IN, pull=Pin.PULL_UP)

# interruption
extint = ExtInt(bp, mode=ExtInt.IRQ_RISING_FALLING,
                pull=Pin.PULL_UP, callback=interrupt_bp)
extint.enable()

print("""
Appuyer sur le bouton-poussoir user (bleu)
ou le bouton-poussoir externe
""")

input("Enter to quit\n")
bp_user.callback(None)  # remove the callback
extint.disable()
print("bye")

""" Résultats :

>>>
Appuyer sur le bouton-poussoir user (bleu)
ou le bouton-poussoir externe

Enter to quit
1 D8 switch
2 D8 switch
3 D8 switch
1 user switch
2 user switch
3 user switch
4 D8 switch
5 D8 switch
4 user switch
5 user switch
6 user switch

bye
>>>
"""
