# MicroPython v1.17 on 2021-10-20; NUCLEO-L476RG with STM32L476RG
# (C) Fabrice SINCERE

# test : OK

from pyb import UART

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

baudrate_reception = 19200  # 2400, 4800, 9600, 19200 etc. 115200 etc.
parity = 1  # None (pas de bit de parité), 1 (impaire), 0 (paire)

# initialisation du port COM
uart1 = UART(1, baudrate=baudrate_reception, bits=8, parity=parity, stop=1,
             flow=0, timeout=0, timeout_char=3, rxbuf=64)


def interruption_reception(obj):
    # réception RX UART1
    reception_bytes = uart1.read()  # type bytes
    try:
        # type str, décodage ascii uniquement
        reception = reception_bytes.decode()
    except AttributeError:
        # cas particulier où il n'y a rien à lire
        return
    except UnicodeError:
        reception = reception_bytes

    print("\nMessage reçu     ({} octets) : {}"
          .format(len(reception), reception))


print("""Transmission série RS232 : partie réception

Carte NUCLEO-L476RG n°2

Broche D2 de la carte n°2 (RX UART1) à relier à la broche D8
de la carte n°1 (TX UART1)
+ GND

Utilisation d'une interruption en réception.

Avec Thonny, il faudra ouvrir deux instances, une par carte.
Pour cela :
Tools -> Options -> General : décocher "Allow only single Thonny instance"
""")

input("Enter pour continuer...")

# activation d'une interruption en réception
uart1.irq(trigger=uart1.IRQ_RXIDLE, handler=interruption_reception)

input("Enter pour quitter...")
uart1.deinit()
print("\nbye !")

# Remarques  :
# Tout se passe bien normalement (cad mêmes paramètres pour les UARTs de la
# carte d'émission et de réception).
# !!! en réception, la parité (paire ou impaire) n'est pas contrôlée ce
# qui est bien dommage
# => avec parités différentes en émission et réception, ça marche quand même...
# On peut cependant vérifier que la transmission est KO si les baudrates
# sont différents.
