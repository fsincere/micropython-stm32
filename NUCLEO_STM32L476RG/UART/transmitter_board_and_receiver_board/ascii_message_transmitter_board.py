# MicroPython v1.17 on 2021-10-20; NUCLEO-L476RG with STM32L476RG
# (C) Fabrice SINCERE

# test : OK

from pyb import UART

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

baudrate_emission = 19200  # 2400, 4800, 9600, 19200 etc. 115200 etc.
parity = 1  # None (pas de bit de parité), 1 (impaire), 0 (paire)

# initialisation du port COM
uart1 = UART(1, baudrate=baudrate_emission, bits=8, parity=parity, stop=1,
             flow=0, timeout=0, timeout_char=3, rxbuf=64)

print("""Transmission série RS232 : partie émission

Carte NUCLEO-L476RG n°1

Broche D2 de la carte n°2 (RX UART1) à relier à la broche D8
de la carte n°1 (TX UART1)
+ GND

Avec Thonny, il faudra ouvrir deux instances, une par carte.
Pour cela :
Tools -> Options -> General : décocher "Allow only single Thonny instance"
""")

input("Enter pour continuer...")
print("CTRL+C pour quitter")

try:
    while True:
        # WARNING: MicroPython ignores non-ascii characters of the input
        # ASCII sur 7 bits...
        while True:
            message = input("Message (ASCII) à transmettre ? ")
            # taille maximale du buffer de réception : rxbuf+1 = 65
            if 0 < len(message) <= 65:
                break

        # émission TX UART1
        size = uart1.write(message)
        print("Message transmis ({} octets) : {}".format(size, message))

except KeyboardInterrupt:
    # CTRL+C
    uart1.deinit()
    print("\nBye !")

# Remarques  :
# Tout se passe bien normalement (cad mêmes paramètres pour les UARTs de la
# carte d'émission et de réception).
# !!! en réception, la parité (paire ou impaire) n'est pas contrôlée ce
# qui est bien dommage
# => avec parités différentes en émission et réception, ça marche quand même...
# On peut cependant vérifier que la transmission est KO si les baudrates
# sont différents.
