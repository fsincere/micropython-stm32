## Carte NUCLEO STM32L476RG : les liaisons série UART

Cette carte dispose de 5 liaisons série UART (Universal Asynchronous Receiver Transmitter) :

- UART(1) : TX (broche Arduino D8)  RX (broche Arduino D2)
- UART(2) : TX (broche Arduino D1)  RX (broche Arduino D0)
- UART(3) : TX (broche Morpho C4)  RX (broche Morpho C5)
- UART(4) : TX (broche Arduino A0)  RX (broche Arduino A1)
- UART(5) : TX (broche Morpho C12)  RX (broche Morpho D2)

Attention, **on ne peut pas utiliser l'UART(2)** car il est connecté au port com virtuel de l'interface ST-LINK qui gère l'interpréteur interactif MicroPython (REPL).  

#### Du point de vue électrique

- Niveaux de tension : 0 (bit 0) et 3.3 V (bit 1)
- Tension de repos : 3.3 V  

#### A propos de l'UART(2) et des broches D1 et D0 du connecteur Arduino

Pour observer les  signaux TX et RX de l'UART(2) qui traduisent la communication REPL entre la carte NUCLEO et un PC via le câble USB, il faut se placer sur les broches cpu.A2 et cpu.A3 (connecteur CN3 côté ST-LINK).  
En réalité, les broches D1 et D0 ne sont pas connectées (cependant, on peut les relier en soudant les solder bridges SB62 and SB63).

### Utilisation basique des UARTs

```python
MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

>>> from pyb import UART
>>> u1 = UART(1, 9600)  # TX broche D8 ; RX broche D2
>>> help(u1)
object UART(1, baudrate=9600, bits=8, parity=None, stop=1, flow=0, timeout=0, timeout_char=3, rxbuf=64) is of type UART
  init -- <function>
  deinit -- <function>
  any -- <function>
  read -- <function>
  readline -- <function>
  readinto -- <function>
  write -- <function>
  irq -- <function>
  writechar -- <function>
  readchar -- <function>
  sendbreak -- <function>
  RTS -- 256
  CTS -- 512
  IRQ_RXIDLE -- 16

>>> u4 = UART(4, 9600)  # TX broche A0 ; RX broche A1
>>> u4
UART(4, baudrate=9600, bits=8, parity=None, stop=1, flow=0, timeout=0, timeout_char=3, rxbuf=64)

>>> # on branche un fil entre D8 et A1, et entre A0 et D2
>>> u1.write("bonjour")
>>> u4.read()
"bonjour"
>>> u4.write("rebonjour")
>>> u1.read()
"rebonjour"
```
