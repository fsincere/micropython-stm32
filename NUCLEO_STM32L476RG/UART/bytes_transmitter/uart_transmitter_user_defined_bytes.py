# MicroPython v1.17 on 2021-10-20; NUCLEO-L476RG with STM32L476RG
# (C) Fabrice SINCERE

from pyb import UART
import time

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

# initialisation du port COM
uart1 = UART(1, baudrate=19200, bits=8, parity=1, stop=1, flow=0, timeout=0,
             timeout_char=3, rxbuf=64)

input("Enter pour continuer...")

print("""Le signal est disponible sur la broche D8
(à visualiser sur un oscilloscope
masse à relier à une broche GND)
""")

datas = bytes([0x94, 0x97, 0x10])  # 3 octets
# datas = b'\x94\x97\x10'

print("Octets transmis : ", datas)

print("CTRL+C pour quitter")

try:
    while True:
        # on répète la transmission des octets
        uart1.write(datas)
        time.sleep_ms(1000)  # pause (ms)

except KeyboardInterrupt:
    # CTRL+C
    print("bye !")
    uart1.deinit()
