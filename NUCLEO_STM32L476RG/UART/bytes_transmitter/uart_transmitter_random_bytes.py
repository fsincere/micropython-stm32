# MicroPython v1.17 on 2021-10-20; NUCLEO-L476RG with STM32L476RG
# (C) Fabrice SINCERE

from pyb import UART
import random

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

# initialisation du port COM
# en pratique baurate max : environ 20000 (pour ne pas avoir de temps mort entre
# l'émission de deux octets successifs)
uart1 = UART(1, baudrate=9600, bits=8, parity=None, stop=1, flow=0, timeout=0,
             timeout_char=3, rxbuf=64)
# print(uart1)
input("Enter pour continuer...")

print("""Random bytes
Le signal est disponible sur la broche D8
(à visualiser sur un oscilloscope
masse à relier à une broche GND)

Spectre FFT pour visualiser la densité spectrale de puissance.
""")

print("CTRL+C pour quitter")

try:
    while True:
        # on transmet un octet de valeur aléatoire
        # test pour un signal rectangulaire f=baudrate/2 :
        # uart1.writechar(random.randint(85, 85))
        uart1.writechar(random.randint(0, 255))
except KeyboardInterrupt:
    # CTRL+C
    print("bye !")
    uart1.deinit()
