# MicroPython v1.19.1 on 2023-02-11; NUCLEO-L476RG with STM32L476RG

from pyb import Pin, LED
import dht  # DHT11, DHT22 sensors
import time

__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

# Sensor DHT11
# 1=VDD 3.3 V, 2=Data, 3=GND

pin = Pin("D7")  # to data DHT11 pin
d = dht.DHT11(pin)

# led built-in
led = LED(1)
led.off()

while True:
    time.sleep_ms(1000)
    try:
        led.on()
        d.measure()
        led.off()
        # Thonny : View -> Plotter
        print("{} °C {} %".format(d.temperature(), d.humidity()))
    except OSError:
        # [Errno 110] ETIMEDOUT
        led.off()
        print("{} °C {} %".format("?", "?"))


""" exemple :
23 °C 57 %
23 °C 57 %
23 °C 57 %
"""
