## Carte NUCLEO STM32L476RG : capteurs DHT11 et DHT22 (AM2302) 

Le DHT11/DHT22 est un capteur numérique de température et d'humidité.  

Pour le DHT22 :

+ résolution numérique de 0,1 °C et 0,1 % pour l'humidité
+ plage (-40 à 80 °C) et (5 à 99 %)

Pour le DHT11 :

+ résolution numérique de 1 °C et 1 % pour l'humidité
+ plages (0 à 50 °C) et (20 à 90 %)

**Module Grove DHT11 :**  
![DHT11-sensor](images/DHT11_sensor_Grove.jpg)  

**Module DHT22 (AM2302) :**  
![DHT22-sensor](images/DHT22-sensor.jpg)  

Le communication utilise un bus série 1-wire.  

3 fils à brancher :

- GND
- Alimentation Vdd 3,3 V
- Broche DATA (SIG) du DHT11/DHT22 à relier à la broche D7 (par exemple) de la carte NUCLEO-L476RG

Une résistance de pull-up d'environ 10 kΩ sur la broche DATA est déjà présente sur le module.  

La durée d'acquisition est assez lente : au mieux 1 mesure toutes les 2 secondes (en pratique, une mesure par seconde).  

Le programme a besoin du module standard ```dht``` (déjà installé, MicroPython 1.19) :  

```python
import dht
```

A défaut, le module dht.py est disponible dans le répertoire /lib (à uploader dans la mémoire flash).  

[Documentation](https://docs.micropython.org/en/latest/esp8266/tutorial/dht.html)