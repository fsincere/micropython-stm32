# MicroPython v1.19.1 on 2023-02-11; NUCLEO-L476RG with STM32L476RG

from pyb import Pin, LED
import dht  # DHT11, DHT22 sensors
import time

__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

# Sensor DHT22 (AM2302)
# 1=VDD 3.3 V, 2=Data, 3=GND

pin = Pin("D7")  # to data DHT22 pin
d = dht.DHT22(pin)

# led built-in
led = LED(1)
led.off()

while True:
    time.sleep_ms(1000)
    try:
        led.on()
        d.measure()  # environ 272 ms
        led.off()
        # Thonny : View -> Plotter
        print("{:.1f} °C {:.1f} %".format(d.temperature(), d.humidity()))
    except OSError:
        # [Errno 110] ETIMEDOUT
        led.off()
        print("{} °C {} %".format("?", "?"))


""" exemple :
19.5 °C 81.1 %
19.5 °C 81.0 %
19.5 °C 81.0 %
19.5 °C 81.0 %
19.5 °C 81.0 %
19.5 °C 81.0 %
"""
