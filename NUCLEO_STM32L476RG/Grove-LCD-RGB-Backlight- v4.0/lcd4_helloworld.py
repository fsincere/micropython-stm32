# (C) Fabrice SINCERE
# MicroPython v1.17 on 2021-10-30; NUCLEO-L476RG with STM32L476RG
# shield grove avec commutateur VCC sur 5V
# afficheur i2c Grove 16x2 LCD RGB backlight v4.0

# test OK

from machine import Pin, I2C
import i2c_lcd4

# bus i2c(1)
# D14 (arduino) pin -> SDA
# D15 (arduino) pin -> SDL
i2c = I2C(1) 

# i2c bus scan
print("Scan du bus I2c :\n")
[print(hex(i)) for i in i2c.scan()]
# 0x3e
# 0x62

d = i2c_lcd4.Display(i2c)

d.clear()
d.home()
d.write('Hello World')
d.move(0, 1)
d.write('Bonjour')

input("Enter to continue")
d.color(255, 127, 0)

input("Enter to continue")
d.autoscroll(True)
d.write("!!!!")

input("Enter to continue")
d.home()
d.autoscroll(False)

# help(i2c_lcd4.Display)
'''
object <class 'Display'> is of type type
  __init__ -- <function __init__ at 0x20003bb0>
  __qualname__ -- Display
  autoscroll -- <function autoscroll at 0x20004170>
  __module__ -- i2c_lcd4
  blink -- <function blink at 0x20004160>
  color -- <function color at 0x200041c0>
  home -- <function home at 0x200041b0>
  cursor -- <function cursor at 0x20004180>
  write -- <function write at 0x20003730>
  clear -- <function clear at 0x200041a0>
  move -- <function move at 0x20004230>
  display -- <function display at 0x20004190>
'''
