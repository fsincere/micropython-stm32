## Afficheur LCD avec rétroéclairage Grove-LCD RGB Backlight v4.0

2x16 caractères

matrice 5x8 pixels

- la carte contient les résistances de pull-up du bus i2c
- adresse i2c screen (afficheur) : 0x3e
- adresse i2c backlight (rétroéclairage) : 0x62


## Code source

github du module MicroPython :

- https://github.com/Bucknalla/micropython-i2c-lcd
- auteur : Alex Bucknall <alex.bucknall@gmail.com> 

J'ai corrigé quelques bugs.

## Documentation du module MicroPython i2c_lcd4

### Méthodes de la classe Display

#### Affichage

clear()

home()

write(text)

- text : type str

cursor(state)

- state : type bool
- underline cursor

blink(state)

- state : type bool
- blinking cursor

autoscroll(state)

- state : type bool
- True : 'right justify' text from the cursor
- False : 'left justify' text from the cursor (default)

display(state)

- state : type bool

move(col, row)

- row : 0 (1ère ligne), 1 (2ème ligne)
- col : 0 (1ère colonne) etc.


#### Rétroéclairage

color(r, g, b)

- composantes rouge, vert, bleu
- 0 à 255

## Remarques

Par rapport à la librairie Arduino (https://github.com/Seeed-Studio/Grove_LCD_RGB_Backlight),  
les fonctions suivantes n'ont pas été implémentées dans la version MicroPython :

- scrollDisplayLeft(), scrollDisplayRight() // these commands scroll the display without changing the RAM
- leftToRight(), rightToLeft() // this is for text that flows Left to Right, Right to Left
- createChar() // allows us to fill the first 8 CGRAM locations with custom characters


(C) Fabrice SINCERE ; version 0.0.3
