## Connexion

### Câble USB ST-Link (accès à l'interpréteur MicroPython avec Thonny)

### Câble USB "user" à fabriquer

Par exemple, récupération du câble USB d'une souris ou d'un clavier hors service.

2 broches seulement :  

- D+ à relier à PA12 (broche 12 CN10)  
- D- à relier à PA11 (broche 14 CN10)  

![USB user](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/USB/images/usb_user_connector.jpg)


## Code

Dans le fichier boot.py, ajouter la ligne :

```python
# pyb.usb_mode([modestr, ]port=-1, vid=0xf055, pid=-1, msc=(), hid=pyb.hid_mouse, high_speed=False)
pyb.usb_mode('VCP+HID', hid=pyb.hid_keyboard)
```

puis Reset de la carte.

Brancher le câble USB "user".

La carte est alors reconnue par votre PC commme un clavier.

Pour s'en convaincre, si vous êtes sous Linux :

```
$ dmesg -wH
[...]
usb 4-1: new full-speed USB device number 2 using ohci-pci
usb 4-1: New USB device found, idVendor=f055, idProduct=9801, bcdDevice= 2.00
usb 4-1: New USB device strings: Mfr=1, Product=2, SerialNumber=3
usb 4-1: Product: Pyboard Virtual Comm Port in FS Mode
usb 4-1: Manufacturer: MicroPython
usb 4-1: SerialNumber: 207837743947
input: MicroPython Pyboard Virtual Comm Port in FS Mode
hid-generic 0003:F055:9801.0006: input,hidraw3: USB HID v1.11 Keyboard [MicroPython Pyboard Virtual Comm Port in FS Mode]
[...]
```

```
$ lsusb -v
Bus 003 Device 002: ID f055:9801 MicroPython Pyboard Virtual Comm Port in FS Mode
Device Descriptor:
    Interface Descriptor:
      bInterfaceClass         3 Human Interface Device
      bInterfaceSubClass      1 Boot Interface Subclass
      bInterfaceProtocol      1 Keyboard
      iInterface              0 
        HID Device Descriptor:
          bLength                 9
          bDescriptorType        33
          bcdHID               1.11
          bCountryCode            0 Not supported
          bNumDescriptors         1
          bDescriptorType        34 Report
          wDescriptorLength      63
[...]
```


