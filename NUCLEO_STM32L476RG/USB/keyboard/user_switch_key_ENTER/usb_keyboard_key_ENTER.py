# (C) Fabrice SINCERE

# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG
# test : OK

import pyb
import time

__version__ = (0, 0, 1)


# pyb.usb_mode([modestr, ]port=-1, vid=0xf055, pid=-1, msc=(),
#              hid=pyb.hid_mouse, high_speed=False)

'''
Dans le fichier boot.py, ajouter la ligne :
pyb.usb_mode('VCP+HID', hid=pyb.hid_keyboard)
'''

'''
le bouton poussoir de la carte donne le même comportement
que la touche ENTER du clavier
'''

# avec une fonction de callback sur le switch, memory error...
sw = pyb.Switch()
hid = pyb.USB_HID()

data = bytearray(8)  # bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00')

print("CTRL+C pour quitter")
try:
    while True:
        while sw():
            # biblio : https://usb.org/sites/default/files/hut1_3_0.pdf
            data[2] = 0x28   # keyboard ENTER 0x28
            # key pressed
            hid.send(data)
            time.sleep_ms(10)  # indispensable
            # key released
            data[2] = 0x00
            hid.send(data)
            time.sleep_ms(300)
except KeyboardInterrupt:
    print("bye!")
