## Carte NUCLEO STM32L476RG

Connexion avec un clavier 4x3 (référence ECO.12150.06.SP)  

![Image](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/keypad/docs/keypad4x3-stm32nucleo.png)


### Bibliographie

[https://github.com/mchobby/esp8266-upy/tree/master/keypad-4x4](https://github.com/mchobby/esp8266-upy/tree/master/keypad-4x4)