# (C) Fabrice SINCERE
# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG
# test : OK

import pyb
import time
# https://framagit.org/fsincere/micropython-stm32/-/tree/main/NUCLEO_STM32L476RG/keypad/lib
from keypad import Keypad4x3

__version__ = (0, 0, 1)


# pyb.usb_mode([modestr, ]port=-1, vid=0xf055, pid=-1, msc=(),
#              hid=pyb.hid_mouse, high_speed=False)

'''
Dans le fichier boot.py, ajouter la ligne :
pyb.usb_mode('VCP+HID', hid=pyb.hid_keyboard)
'''


def keypress(key):
    # trame usb
    try:
        data[2] = keypadID[key]
    except KeyError:
        return
    # key pressed
    hid.send(data)
    time.sleep_ms(10)  # indispensable
    # key released
    data[2] = 0x00
    hid.send(data)


# lines : Y1, Y2, Y3, Y4
# cols  : X1, X2, X3
k = Keypad4x3(lines=["D4", "D5", "D6", "D7"],
              cols=["D8", "D9", "D10"])

hid = pyb.USB_HID()

# biblio : https://usb.org/sites/default/files/hut1_3_0.pdf
keypadID = {
 '1': 0x59,
 '2': 0x5a,
 '3': 0x5b,
 '4': 0x5c,
 '5': 0x5d,
 '6': 0x5e,
 '7': 0x5f,
 '8': 0x60,
 '9': 0x61,
 '0': 0x62}

data = bytearray(8)  # bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00')

print("CTRL+C pour quitter")
try:
    while True:
        # timeout=None for infinite timeout
        key = k.read_key(timeout=None)
        # print(key)
        keypress(key)
        time.sleep_ms(300)
except KeyboardInterrupt:
    print("bye!")
