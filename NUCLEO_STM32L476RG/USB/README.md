## Connexion

### Câble USB ST-Link (accès à l'interpréteur MicroPython avec Thonny)

### Câble USB "user" à fabriquer

Par exemple, récupération du câble USB d'une souris ou d'un clavier hors service.

2 broches utiles :  

- D+ à relier à PA12 (broche 12 CN10)  
- D- à relier à PA11 (broche 14 CN10)  

![USB user](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/USB/images/usb_user_connector.jpg)




