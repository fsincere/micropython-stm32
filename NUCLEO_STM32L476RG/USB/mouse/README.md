## Connexion

### Câble USB ST-Link (accès à l'interpréteur MicroPython avec Thonny)

### Câble USB "user" à fabriquer

Par exemple, récupération du câble USB d'une souris ou d'un clavier hors service.

2 broches utiles :  

- D+ à relier à PA12 (broche 12 CN10)  
- D- à relier à PA11 (broche 14 CN10)  

![USB user](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/USB/images/usb_user_connector.jpg)


## Code

Dans le fichier boot.py, décommenter la ligne :

```python
# pyb.usb_mode('VCP+HID')
```

puis Reset de la carte.

Brancher le câble USB "user".

La carte est alors reconnue par votre PC comme une souris.

Pour s'en convaincre, si vous êtes sous Linux :

```
$ dmesg -wH
[...]
input: MicroPython Pyboard Virtual Comm Port in FS Mode
hid-generic 0003:F055:9801.0006: input,hidraw3: USB HID v1.11 Mouse [MicroPython Pyboard Virtual Comm Port in FS Mode]
[...]
```

Dans Thonny :

```python
>>> hid = pyb.USB_HID()
>>> hid.send((0, 100, 0, 0)) # (button status, x-direction, y-direction, scroll)
```

Le pointeur de la souris se déplace de 100 pixels vers la droite.


Webographie :  [Making the stm32 board act as a USB mouse](https://docs.micropython.org/en/latest/pyboard/tutorial/usb_mouse.html)


