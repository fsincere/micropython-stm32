# (C) Fabrice SINCERE

# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG
# test OK

import pyb
import random
import time

__version__ = (0, 0, 2)

'''
Dans le fichier boot.py, décommenter la ligne :
# pyb.usb_mode('VCP+HID')
'''

'''
Provoque un mouvement aléatoire de la souris
(activation par le bouton poussoir)
'''

# avec une fonction de callback sur le switch, memory error...
sw = pyb.Switch()
hid = pyb.USB_HID()

print("CTRL+C pour quitter")
try:
    while True:
        while sw():
            # (button status, x-direction, y-direction, scroll)
            hid.send((0, random.randint(-5, 5), random.randint(-10, 10), 0))
            time.sleep_ms(20)
except KeyboardInterrupt:
    print("bye!")
