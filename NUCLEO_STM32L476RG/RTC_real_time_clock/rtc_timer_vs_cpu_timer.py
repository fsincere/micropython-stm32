# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG
# Horloge temps réel
# RTC interne du cpu avec quartz externe 32768 Hz (présent sur la carte)

''' Exemple d'utilisation du timer associé à l'horloge temps réel (RTC)
Cela permet d'avoir une base de temps de grande précision
(liée à la tolérance en fréquence du quartz externe 32768 Hz,
avec une dérive typique de quelques minutes par an).

La précision sur les timers pyb.Timer ou machine.Timer est beaucoup
plus faible car liée à la tolérance de l'oscillateur interne du cpu :
expérimentalement, j'ai mesuré une dérive d'environ 2 secondes par heure
'''

# à la mise sous tension :
# (year, month, day, weekday, hours, minutes, seconds, subseconds)
# subseconds counts down from 255 to 0
# (2015, 1, 1, 4, 0, 0, 0, 0)

import micropython
import pyb
import machine
import time

__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

micropython.alloc_emergency_exception_buf(100)


def cb1(e):
    'rtc timer callback function'
    global counter1
    print('rtc timer', counter1, rtc.datetime())
    counter1 += 1


def cb2(e):
    'machine timer callback function'
    global counter2
    print('machine timer', counter2, rtc.datetime())
    counter2 += 1


counter1 = 0
counter2 = 0

rtc = pyb.RTC()
# set the RTC wakeup timer
# RTC.wakeup(timeout, callback=None)
rtc.wakeup(1000, cb1)  # 1000 ms

time.sleep(0.5)

timer = machine.Timer()
timer.init(mode=machine.Timer.PERIODIC, period=1000, callback=cb2)

input("Enter to quit\n")
# disable the RTC wakeup timer
rtc.wakeup(None)
# disable cpu timer
timer.deinit()
