## Carte NUCLEO STM32L476RG : horloge temps réel RTC

La carte NUCLEO possède une horloge temps réel (RTC interne du cpu avec quartz externe 32768 Hz présent sur la carte).  

Cela permet d'avoir une base de temps de grande précision, liée à la tolérance en fréquence du quartz externe 32768 Hz, avec une dérive typique de quelques minutes par an.  

Evidemment, en cas de coupure de l'alimentation, l'horloge RTC est réinitialisée à sa valeur par défaut (2015-01-01 00:00:00).  

Avec Thonny, vous avez la possibilité de synchroniser l'horloge RTC avec l'heure locale du PC :)


![Thonny IDE synchronize device's RTC](images/Thonny IDE synchronize device RTC.png)

