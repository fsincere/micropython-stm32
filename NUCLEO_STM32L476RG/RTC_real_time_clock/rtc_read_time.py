# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG
# Horloge temps réel
# RTC interne avec quartz 32768 Hz

# (C) Fabrice Sincère
# test : OK

# à la mise sous tension :
# (year, month, day, weekday, hours, minutes, seconds, subseconds)
# (2015, 1, 1, 4, 0, 0, 0, 0)

import pyb
import time

__version__ = (0, 0, 4)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

day_of_week = {1: 'Monday', 2: 'Tuesday', 3: 'Wednesday', 4: 'Thursday',
               5: 'Friday', 6: 'Saturday', 0: 'Sunday'}

# get time
rtc = pyb.RTC()
print(rtc.datetime())

print("CTRL+C to quit")

while True:
    time.sleep(1)
    # tuple
    year, month, day, day_nb, hour, minute, second, subseconds = rtc.datetime()
    # format
    # (2023, 1, 29, 7, 10, 28, 33, 154)
    # 2023-01-29 Sunday 10:28:33
    print("{:04d}-{:02d}-{:02d} {} {:02d}:{:02d}:{:02d}"
          .format(year, month, day, day_of_week[day_nb%7], hour, minute, second))
