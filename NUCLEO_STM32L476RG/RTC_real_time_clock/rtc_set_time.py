# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG
# Horloge temps réel
# RTC interne avec quartz 32768 Hz

# (C) Fabrice Sincère
# test : OK

# à la mise sous tension :
# (2015, 1, 1, 4, 0, 0, 0, 0)

import pyb

__version__ = (0, 0, 4)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

rtc = pyb.RTC()

year = int(input("year YYYY ? "))
month = int(input("month (1-12) ? "))
day = int(input("day of the month (1-31) ? "))
day_nb = int(input("day of the week (Monday->1 Sunday->7) ? "))
hour = int(input("hour (0-23) ? "))
minute = int(input("minute (0-59) ? "))
second = int(input("second (0-59) ? "))

t = (year, month, day, day_nb, hour, minute, second, 0)
# set time
rtc.datetime(t)
# read time
print(rtc.datetime())
