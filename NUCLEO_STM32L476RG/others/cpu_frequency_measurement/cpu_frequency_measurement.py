# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

import micropython
from pyb import DAC, Timer

__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

micropython.alloc_emergency_exception_buf(100)

print("""Mesure de la fréquence du CPU
Brancher un fréquencemètre étalon sur la sortie A2
(et un oscilloscope pour contrôler la forme du signal).
""")

# 80 MHz avec une tolérance constructeur de 0.25 %
fcpu = pyb.freq()[0]
print("Fréquence nominale du CPU : {} MHz".format(fcpu/1e6))

# timer associé au DAC
# f = fcpu/(period+1)
# 200 kHz avec period = 399
tim = Timer(6, prescaler=0, period=399)
f = tim.freq()

dac = DAC(1)  # sortie A2
dac.init(bits=8, buffering=True)
# on génère un créneau 0/3.3V de fréquence f/2
buffer = bytearray(b'\x00\xff')
dac.write_timed(buffer, tim, mode=DAC.CIRCULAR)

print("Fréquence nominale du signal sur la broche A2 : {} Hz".format(f/2))
fmeas = float(input("Fréquence mesurée (Hz) ? "))  # Hz
ecart = 2*(fmeas-f/2)/f
print("Ecart : {:+} %".format(ecart*100))

print("Fréquence CPU :", fcpu*(1+ecart)*1e-6, "MHz")

input("Enter to quit")
dac.deinit()

""" Résultat :
>>>
Mesure de la fréquence du CPU
Brancher un fréquencemètre étalon sur la sortie A2
(et un oscilloscope pour contrôler la forme du signal).

Fréquence nominale du CPU : 80.0 MHz
Fréquence nominale du signal sur la broche A2 : 100000.0 Hz
Fréquence mesurée (Hz) ? 99944
Ecart : -0.056 %
Fréquence CPU : 79.9552 MHz
Enter to quit
>>>
"""

"""
Remarques :
On a bien 0.056 < 0.25 %
Cela correspond à un décalage de 2 secondes par heure
"""
