# Updated 2024   Fabrice Sincère
# MicroPython v1.19.1 on 2023-02-11; NUCLEO-L476RG with STM32L476RG
# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

# source : Robert Hammelrath
# https://github.com/robert-hh/BME280
# source : Adafruit
# https://github.com/adafruit/Adafruit_CircuitPython_BME280

import time
from ustruct import unpack
from array import array

__version__ = (0, 1, 9)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

# BMP280 default i2c address
BMP280_I2CADDR = const(0x76)

# Registers address
BMP280_REGISTER_STATUS = const(0xF3)
BMP280_REGISTER_CONTROL = const(0xF4)
BMP280_REGISTER_CONFIG = const(0xF5)
BMP280_REGISTER_RESET = const(0xE0)
BMP280_REGISTER_ID = const(0xD0)

# Oversampling
# temperature bit 7, 6, 5 BMP280_REGISTER_CONTROL
# pression    bit 4, 3, 2 BMP280_REGISTER_CONTROL
BMP280_OSAMPLE_OFF = const(0x00)    # measurement skipped
BMP280_OSAMPLE_1 = const(0x01)    # oversampling ×1
BMP280_OSAMPLE_2 = const(0x02)    # oversampling ×2
BMP280_OSAMPLE_4 = const(0x03)   # oversampling ×4
BMP280_OSAMPLE_8 = const(0x04)    # oversampling ×8
BMP280_OSAMPLE_16 = const(0x05)   # oversampling ×16

BMP280_OSAMPLE = {
    BMP280_OSAMPLE_OFF: 0,
    BMP280_OSAMPLE_1: 1,
    BMP280_OSAMPLE_2: 2,
    BMP280_OSAMPLE_4: 4,
    BMP280_OSAMPLE_8: 8,
    BMP280_OSAMPLE_16: 16
}

# bit 1 bit 0 BMP280_REGISTER_CONTROL
# no operation, all registers accessible, lowest power, selected after startup
MODE_SLEEP = const(0x00)
# perform one measurement, store results and return to sleep mode
MODE_FORCED = const(0x01)
# perpetual cycling of measurements and inactive periods
MODE_NORMAL = const(0x03)

# iir low_pass filter coefficient
# bit 4, 3, 2 BMP280_REGISTER_CONFIG
# temperature and pressure only
BMP280_IIR_OFF = const(0x00)  # filter off
BMP280_IIR_2 = const(0x01)    # filter coefficient 2
BMP280_IIR_4 = const(0x02)    # filter coefficient 4
BMP280_IIR_8 = const(0x03)    # filter coefficient 8
BMP280_IIR_16 = const(0x04)   # filter coefficient 16

# standby timeconstant values
# Controls inactive duration t standby in normal mode
# bit 7, 6, 5 BMP280_REGISTER_CONFIG
BMP280_STANDBY_0_5 = const(0x00)   # 0.5 ms
BMP280_STANDBY_62_5 = const(0x01)  # 62.5 ms
BMP280_STANDBY_125 = const(0x02)   # 125 ms
BMP280_STANDBY_250 = const(0x03)   # 250 ms
BMP280_STANDBY_500 = const(0x04)   # 500 ms
BMP280_STANDBY_1000 = const(0x05)  # 1000 ms
BMP280_STANDBY_2000 = const(0x06)  # 2000 ms
BMP280_STANDBY_4000 = const(0x07)  # 4000 ms

BMP280_STANDBY = {
    BMP280_STANDBY_0_5: 0.5,
    BMP280_STANDBY_62_5: 62.5,
    BMP280_STANDBY_125: 125,
    BMP280_STANDBY_250: 250,
    BMP280_STANDBY_500: 500,
    BMP280_STANDBY_1000: 1000,
    BMP280_STANDBY_2000: 2000,
    BMP280_STANDBY_4000: 4000
}


class BMP280:

    def __init__(self,
                 # oversampling=(BMP280_OSAMPLE_1, BMP280_OSAMPLE_1)
                 oversampling=BMP280_OSAMPLE_1,
                 iir=BMP280_IIR_OFF,
                 standby=BMP280_STANDBY_125,
                 address=BMP280_I2CADDR,
                 i2c=None):

        self.address = address

        if self.address not in [0x76, 0x77]:
            raise ValueError("Invalid i2c address")

        if i2c is None:
            raise ValueError('An I2C object is required.')
        self.i2c = i2c

        # Check that oversampling is valid.
        if type(oversampling) is tuple and len(oversampling) == 2:
            self._oversampling_temp, self._oversampling_press = oversampling
        elif type(oversampling) == int:
            self._oversampling_temp, self._oversampling_press = \
                oversampling, oversampling
        else:
            raise ValueError("Wrong type for the oversampling parameter, \
must be int or a 2 element tuple")

        for oversampling in (self._oversampling_temp, self._oversampling_press):
            if oversampling not in [BMP280_OSAMPLE_OFF, BMP280_OSAMPLE_1,
                                    BMP280_OSAMPLE_2, BMP280_OSAMPLE_4,
                                    BMP280_OSAMPLE_8, BMP280_OSAMPLE_16]:
                raise AttributeError()

        # verify chip ID
        self.verify_chip_ID()

        # reset  => set SLEEP MODE
        self.i2c.writeto_mem(self.address, BMP280_REGISTER_RESET,
                             bytearray([0xB6]))
        time.sleep_ms(10)

        # set config register
        # iir filter coefficient
        self._iir_filter = iir
        # standby time
        self._t_standby = standby   # ms
        config = 0x00
        config += self._t_standby << 5
        config += self._iir_filter << 2
        self.i2c.writeto_mem(self.address, BMP280_REGISTER_CONFIG,
                             bytearray([config]))

        self.__sealevel = 101325  # Pascal

        # load calibration data
        dig_88_9f = self.i2c.readfrom_mem(self.address, 0x88, 24)
        self.dig_T1, self.dig_T2, self.dig_T3, self.dig_P1, \
            self.dig_P2, self.dig_P3, self.dig_P4, self.dig_P5, \
            self.dig_P6, self.dig_P7, self.dig_P8, self.dig_P9 \
            = unpack("<HhhHhhhhhhhh", dig_88_9f)

        self.t_fine = 0

        # temporary data holders which stay allocated
        self._l1_barray = bytearray(1)
        self._l6_barray = bytearray(6)
        self._l2_resultarray = array("i", [0, 0])

        # set control register ; set MODE NORMAL
        self._l1_barray[0] = \
            self._oversampling_temp << 5 | self._oversampling_press << 2 | MODE_NORMAL
        self.i2c.writeto_mem(self.address, BMP280_REGISTER_CONTROL,
                             self._l1_barray)

    def read_raw_data(self, result):
        """ Reads the raw (uncompensated) data from the sensor.

            Args:
                result: array of length 2 or alike where the result will be
                stored, in temperature, pressure order
            Returns:
                None"""

        # Wait for conversion to complete
        # while self.get_flag_conversion():
        #    time.sleep_ms(10)  # still busy

        # burst readout from 0xF7 to 0xFC, recommended by datasheet
        self.i2c.readfrom_mem_into(self.address, 0xF7, self._l6_barray)
        readout = self._l6_barray
        # pressure(0xF7): ((msb << 16) | (lsb << 8) | xlsb) >> 4
        raw_press = ((readout[0] << 16) | (readout[1] << 8) | readout[2]) >> 4
        # temperature(0xFA): ((msb << 16) | (lsb << 8) | xlsb) >> 4
        raw_temp = ((readout[3] << 16) | (readout[4] << 8) | readout[5]) >> 4

        result[0] = raw_temp
        result[1] = raw_press

    def read_compensated_data(self, result=None, raw_data=None, printfloat=False):
        """ Reads the data from the sensor and returns the compensated data.

            Args:
                result: array of length 2 or alike where the result will be
                stored, in temperature, pressure order. You may use
                this to read out the sensor without allocating heap memory

            Returns:
                array with temperature, pressure. Will be the one
                from the result parameter if not None
        """
        if raw_data:
            raw_temp, raw_press = raw_data
        else:
            # raw_data is None
            self.read_raw_data(self._l2_resultarray)
            raw_temp, raw_press = self._l2_resultarray

        # temperature
        var1 = (((raw_temp // 8) - (self.dig_T1 * 2)) * self.dig_T2) // 2048
        var2 = (raw_temp // 16) - self.dig_T1
        var2 = (((var2 * var2) // 4096) * self.dig_T3) // 16384
        self.t_fine = var1 + var2
        temp = (self.t_fine * 5 + 128) // 256

        # pressure
        var1 = self.t_fine - 128000
        var2 = var1 * var1 * self.dig_P6
        var2 = var2 + ((var1 * self.dig_P5) << 17)
        var2 = var2 + (self.dig_P4 << 35)
        var1 = (((var1 * var1 * self.dig_P3) >> 8) +
                ((var1 * self.dig_P2) << 12))
        var1 = (((1 << 47) + var1) * self.dig_P1) >> 33
        if var1 == 0:
            pressure = 0
        else:
            p = ((((1048576 - raw_press) << 31) - var2) * 3125) // var1
            var1 = (self.dig_P9 * (p >> 13) * (p >> 13)) >> 25
            var2 = (self.dig_P8 * p) >> 19
            pressure = ((p + var1 + var2) >> 8) + (self.dig_P7 << 4)

        if result:
            result[0] = temp
            result[1] = pressure

            if printfloat:
                res = "{}.{:02d}".format(result[0] // 100, 100*(result[0] % 100) // 100)
                res += " {}.{:08d}".format(result[1] // 256, 100000000*(result[1] % 256) // 256)
                print(res)

            return result

        # result is None
        if printfloat:
            res = "{}.{:02d}".format(temp // 100, 100*(temp % 100) // 100)
            res += " {}.{:08d}".format(pressure // 256, 100000000*(pressure % 256) // 256)
            print(res)

        return array("i", (temp, pressure))

    @property
    def cycle_time(self):
        """ return cycle_time (ms) in normal mode
(standby time + maximum time required to complete a measurement)"""

        meas_time_ms = 1.25
        if self._oversampling_temp != BMP280_OSAMPLE_OFF:
            meas_time_ms += 2.3 * BMP280_OSAMPLE[self._oversampling_temp]
        if self._oversampling_press != BMP280_OSAMPLE_OFF:
            meas_time_ms += 2.3*BMP280_OSAMPLE[self._oversampling_press]+0.575
        return int(meas_time_ms + BMP280_STANDBY[self._t_standby])

    def verify_chip_ID(self):
        """The id register contains the chip identification number which is
0x56 / 0x57 (samples) 0x58 (mass production)"""

        ID_register = self.i2c.readfrom_mem(self.address, BMP280_REGISTER_ID, 1)
        if ID_register[0] == 0x60:
            raise ValueError("It's not a bmp280 sensor, but a bme280 sensor")
        elif ID_register[0] in [0x56, 0x57, 0x58]:
            return
        else:
            raise ValueError("Unknown device")

    def get_config_register(self):
        """Read and return the configuration register"""
        config_register = \
            self.i2c.readfrom_mem(self.address, BMP280_REGISTER_CONFIG, 1)
        return config_register[0]

    def get_flag_conversion(self):
        """Automatically set to 1 whenever a conversion is running
and back to 0 when the results have been transferred
to the data registers"""
        # read bit 3 Register 0xF3 status
        return self.i2c.readfrom_mem(self.address, BMP280_REGISTER_STATUS, 1)[0] & 0x08

    @property
    def temperature_compensation_values(self):
        """
return dig_T1, dig_T2, dig_T3
"""
        return self.dig_T1, self.dig_T2, self.dig_T3

    @property
    def pressure_compensation_values(self):
        """
return dig_P1, dig_P2, dig_P3, dig_P4, dig_P5, dig_P6, dig_P7, dig_P8, dig_P9
"""
        return self.dig_P1, \
            self.dig_P2, self.dig_P3, self.dig_P4, self.dig_P5, \
            self.dig_P6, self.dig_P7, self.dig_P8, self.dig_P9

    @property
    def sealevel(self):
        return self.__sealevel

    @sealevel.setter
    def sealevel(self, value):
        if 30000 < value < 120000:  # just ensure some reasonable value
            self.__sealevel = value

    @property
    def altitude(self):
        '''
        return altitude in m.
        '''
        try:
            altitude = 44330 * (1.0 - ((self.read_compensated_data()[1] / 256) /
                                self.__sealevel)**0.1903)
        except ValueError:
            altitude = 0.0
        return altitude

    @staticmethod
    def pressure_to_altitude(pressure, sealevel=101325):
        '''
        pressure in Pascal
        return altitude in m.
        '''
        return 44330*(1.0-(pressure/sealevel)**0.1903)

    @staticmethod
    def altitude_to_pressure(altitude, sealevel=101325):
        '''
        Altitude in m.
        return pressure in Pascal
        '''
        if altitude < 44330:
            return sealevel*(1-altitude/44330)**5.255
        return 0.0

    def values(self, compensated_data=None):
        """ human readable values """

        if compensated_data:
            t, p = compensated_data
        else:
            # compensated_data is None
            t, p = self.read_compensated_data()
        p /= 256
        return ("{:.02f} C".format(t/100), "{:.03f} hPa".format(p/100))
