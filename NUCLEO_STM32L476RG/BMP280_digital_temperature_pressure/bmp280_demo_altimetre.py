# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG
# test OK

from machine import I2C
from bmp280 import *
from time import sleep_ms

__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

# bus i2c(1)
# D14 (arduino) pin -> SDA
# D15 (arduino) pin -> SCL
i2c = I2C(1)

# i2c bus scan
print("I2c bus scan :")
[print(hex(i)) for i in i2c.scan()]

# oversampling = oversampling_temp, oversampling_press
# iir low-pass filter coefficient
# standy time (ms)

bmp = BMP280(oversampling=(BMP280_OSAMPLE_1, BMP280_OSAMPLE_16),
             iir=BMP280_IIR_16,
             standby=BMP280_STANDBY_0_5,
             i2c=i2c, address=0x76)

cycle_time = bmp.cycle_time
print("cycle time :", cycle_time, "ms")

# pressure altitude 0 m, Pascal
bmp.sealevel = 101325+720  # value to adjust

print("Altitude (m)")
print("CTRL+C to quit")
sleep_ms(1000)

while True:
    print(bmp.altitude)
    sleep_ms(cycle_time)

''' result :
I2c bus scan :
0x76
cycle time : 41 ms
Altitude (m)
CTRL+C to quit
84.56598
84.56862
84.55276
'''