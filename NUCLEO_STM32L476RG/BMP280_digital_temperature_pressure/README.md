## Carte NUCLEO STM32L476RG : capteur BMP280 (Bosch Sensortec)

Le BMP280 est un capteur numérique de température et pression atmosphérique.  
Il fait partie de la même famille que le [BME280](https://framagit.org/fsincere/micropython-stm32/-/tree/main/NUCLEO_STM32L476RG/BME280_digital_temperature_pressure_humidity).  

#### Plage de mesures

+ Température -40 à +85 °C
+ Pression 300 à 1100 hPa

#### Précision des mesures

+ Température ±1,0 °C (0 … 65 °C)
+ Pression ± 1,0 hPa (300 ... 1100 hPa ; 0 ... 65 °C)

![BMP280-sensor](../BME280_digital_temperature_pressure_humidity/images/BME280-sensor.jpg)

Sur cette carte, le capteur est entouré de quelques résistances et condensateurs :  

![BMP280-schematic](../BME280_digital_temperature_pressure_humidity/images/BME280-schematic.jpg)


Le BMP280 possède une interface SPI et une interface I2C.  

Ici, il ne sera question que du bus i2c.  

Deux adresses I2C :   

- par défaut : 0x76  (SDO non connectée,  0 V)  
- 0x77  (SDO reliée à 3,3 V)  

4 fils à brancher :

- GND
- Alimentation Vcc 3,3 V
- Broche SDA du BMP280 à relier à la broche D14 de la carte NUCLEO-L476RG
- Broche SCL du BMP280 à relier à la broche D15 de la carte NUCLEO-L476RG

Pas besoin de résistances de pull-up sur les broches SDA et SCL car elles sont déja présentes sur la carte BMP280 (R2 et R3, 10 kΩ).


## Le driver i2c MicroPython pour le capteur BMP280

Le module MicroPython bmp280.py gère la communication i2c avec le capteur BMP280.  

Je l'ai adapté des deux projets suivants :  

[https://github.com/robert-hh/BME280](https://github.com/robert-hh/BME280)  

[https://github.com/adafruit/Adafruit_CircuitPython_BME280](https://github.com/adafruit/Adafruit_CircuitPython_BME280)  


### Utilisation

On commence pour uploader dans la mémoire flash du microcontrôleur STM32L476RG le module bmp280.py  

A noter que seul le mode "normal" du BMP280 est proposé par ce module (dans ce mode, les mesures sont périodiques).  

```Python
>>> from machine import I2C
>>> from bmp280 import *

>>> i2c = I2C(1)

>>> bmp = BMP280(oversampling=BMP280_OSAMPLE_2,
                 iir=BMP280_IIR_OFF,
                 standby=BMP280_STANDBY_125,
                 i2c=i2c, address=0x76)

>>> print(bmp.values())
('21.33 C', '997.146 hPa')
```

### Bruit de mesures

Voici le résultat de 500 mesures de pression (avec oversampling x2 sans filtre iir) :  

![pressure_500ech_x2_no_iir_chronogramme](../BME280_digital_temperature_pressure_humidity/images/pressure_500ech_x2_no_iir_chrono.png)

![pressure_500ech_x2_no_iir](../BME280_digital_temperature_pressure_humidity/images/pressure_500ech_x2_no_iir.png)

Moyenne	: 25279599,3/256 = 98748,43 Pa  

RMS noise (écart-type) : 2,58 Pa (2,6 Pa d'après le datasheet :)  

On constate que le bruit est gaussien (loi normale).  

### Oversampling

L'oversampling peut prendre les valeurs x1, x2, x4, x8 ou x16 (BMP280_OSAMPLE_1, BMP280_OSAMPLE_2, BMP280_OSAMPLE_4, BMP280_OSAMPLE_8 ou BMP280_OSAMPLE_16).  

x1 pour une mesure de base.  
xN revient à faire une moyenne de N mesures, d'où une meilleure résolution et une réduction du bruit.  
Par contre, la durée de conversion est N fois plus grande.  

On peut individualiser l'oversampling des mesures de température et pression (dans cet ordre) :  
```Python
oversampling=(BMP280_OSAMPLE_8, BMP280_OSAMPLE_16),
```
On peut aussi désactiver certaines mesures.  
Par exemple, pour ignorer la mesure de pression :  
```Python
oversampling=(BMP280_OSAMPLE_8, BMP280_OSAMPLE_OFF),
```

### IIR low-pass filter

On peut activer un filtre numérique récursif passe-bas interne.  

L'équation de récurrence (Cf. datasheet du BMP280) est :  

```
y(n) = (y(n-1)*(a-1) + x(n))/a
```
x(n) désigne la dernière valeur du convertisseur ADC, et y(n) la sortie du filtre numérique.  

Le coefficient *a* peut prendre les valeurs 2, 4, 8 ou 16 (iir=BMP280_IIR_2, BMP280_IIR_4, BMP280_IIR_8 ou BMP280_IIR_16).  

Quand le coefficient *a* augmente, la fréquence de coupure du filtre diminue, le lissage des mesures est plus efficace et cela se traduit également par une diminution du bruit.  
Par contre, le temps de réponse augmente.  

Pour désactiver le filtre interne :  
```Python
iir = BMP280_IIR_OFF,
```

### Résolution du convertisseur ADC

Pour les mesures de température et de pression, la résolution va de 16 bits (oversampling x1) à 20 bits (oversampling x16).

### Correction des mesures

Pour une meilleure précision, les données brutes (raw datas) fournies par le convertisseur ADC doivent être corrigées (compensation de l'effet de la température sur la mesure de la pression).  
La procédure utilisée est celle proposée par le fabricant et elle donne un résultat sur 32 bits.  
Elle tient compte des données de calibration interne propre à chaque BMP280 (Cf. datasheet).  

```Python
>>> temperature, pressure = bmp.read_compensated_data()

>>> print(temperature, pressure)
2172 25525449
```
La température s'obtient ensuite en divisant par 100 : 2172/100 = 21,72 °C,  
et la pression en divisant par 256 : 25525449/256 = 99708.78515625 Pa,  

```Python
>>> print(temperature/100, pressure/256)
21.72 99708.78
```
Ci-dessus, les calculs sont donnés avec la précision machine, soit environ 7 chiffres significatifs (float).  
Pour un calcul exact :  

```Python
>>> temperature, pressure = bmp.read_compensated_data(printfloat=True)
21.72 99708.78515625
```

La même chose avec un format différent :  
```Python
>>> print(bmp.values())
('21.72 C', '997.088 hPa')
```

Pour information, on peut lire les données brutes du convertisseur ADC de la manière suivante :  


```Python
>>> from array import array
>>> result = array("i", [0, 0])
>>> bmp.read_raw_data(result)
>>> raw_temp, raw_press = result
>>> print(raw_temp, raw_press)
510161 290632
```

Puis, pour obtenir les valeurs compensées correspondantes :  

```Python
>>> temperature, pressure = bmp.read_compensated_data(raw_data=(raw_temp, raw_press), printfloat=True)
21.74 99700.67968750
>>> print(temperature, pressure)
2174 25523374
>>> values = bmp.values(compensated_data=(temperature, pressure))
>>> print(values)
('21.74 C', '997.007 hPa')
```


### Standby time

C'est le temps de pause entre deux conversions (on est en mode normal).  

8 valeurs possibles :  

```python
standby=
BMP280_STANDBY_0_5   # 0.5 ms
BMP280_STANDBY_62_5  # 62.5 ms
BMP280_STANDBY_125   # 125 ms
BMP280_STANDBY_250   # 250 ms
BMP280_STANDBY_500   # 500 ms
BMP280_STANDBY_1000  # 1000 ms
BMP280_STANDBY_2000  # 2000 ms
BMP280_STANDBY_4000  # 4000 ms
```

### Fréquence d'échantillonnage

La durée d'un cycle de mesures est l'addition de la durée d'acquisition et de la durée de standby.  

La durée d'acquisition ne dépend que du choix de l'oversampling.  

Avec un oversampling x2 (oversampling=BMP280_OSAMPLE_2) et un standby de 125 ms (standby=BMP280_STANDBY_125), cela donne :

```Python
>>> cycle_time = bmp.cycle_time
>>> print("cycle time :", cycle_time, "ms")
cycle time : 136 ms
```
soit 8 mesures par seconde.  

La durée d'acquisition vaut donc 136-125 = 11 ms (pour l'ensemble des 2 mesures).  

Pour échantillonner au plus vite, on choisira oversampling x1 et standby de 0,5 ms. La durée d'un cycle vaut alors 6 ms (environ 160 mesures par seconde).  

Si on veut un bruit minimal, on choisira un oversampling x16 et un filtrage numérique avec coefficient 16.  

### Altimètre

La pression atmosphérique diminue avec l'altitude.  
On connaît la relation (approximative) entre ces deux grandeurs, on peut alors estimer l'altitude absolue à partir de la mesure de la pression.  

```Python
>>> bmp.sealevel = 101325  # en pascal # à ajuster
>>> print(bmp.values())
('20.01 C', '1010.430 hPa')
>>> print(bmp.altitude)  # en mètre
23.50567
```

La mesure de variation d'altitude (hauteur ou dénivellation) est par contre beaucoup plus précise : la précision est liée au bruit de mesure soit 0,2 Pa dans le meilleur des cas. Cela correspond à une incertitude de 1,7 cm (variation d'environ -12 Pa/m).  

On peut donc espérer faire des mesures à quelques centimètres près...  

Un exemple concret en élevant le capteur de 73 cm (script *bmp280_demo_altimetre.py*) :  

![altimetre](../BME280_digital_temperature_pressure_humidity/images/altimetre.png)


En complément :  
```Python
>>> from bmp280 import *
>>> BMP280.pressure_to_altitude(pressure=99000, sealevel=101325)
195.3961
>>> BMP280.altitude_to_pressure(altitude=195.39, sealevel=101325)
99000.0
```

### Lien utile

[https://github.com/boschsensortec/BMP2_SensorAPI](https://github.com/boschsensortec/BMP2_SensorAPI)
