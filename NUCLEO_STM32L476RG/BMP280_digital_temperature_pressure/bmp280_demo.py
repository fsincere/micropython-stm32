# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG
# test OK

from machine import I2C
from bmp280 import *
from time import sleep_ms

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

# bus i2c(1)
# D14 (arduino) pin -> SDA
# D15 (arduino) pin -> SCL
i2c = I2C(1)

# i2c bus scan
print("I2c bus scan :")
[print(hex(i)) for i in i2c.scan()]

# oversampling = oversampling_temp, oversampling_press
# iir low-pass filter coefficient
# standy time (ms)

bmp = BMP280(oversampling=(BMP280_OSAMPLE_1, BMP280_OSAMPLE_16),
             iir=BMP280_IIR_16,
             standby=BMP280_STANDBY_125,
             i2c=i2c, address=0x76)

cycle_time = bmp.cycle_time
print("cycle time :", cycle_time, "ms")

print("CTRL+C to quit")
sleep_ms(1000)

while True:
    # temperature, pressure = bmp.read_compensated_data()
    # temperature : value 2534 <=> 25.34 °C
    # pressure : 24674867 <=>  24674867/256 = 96386.2 Pa
    # print(temperature, pressure)
    print(bmp.values())
    sleep_ms(cycle_time)

""" result :
I2c bus scan :
0x76
cycle time : 165 ms
CTRL+C to quit
('21.12 C', '997.554 hPa')
('21.12 C', '997.555 hPa')
('21.13 C', '997.547 hPa')
"""
