# (C) Fabrice SINCERE

# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

# test OK avec buzzer grove v1.2 (buzzer commandé par un transistor)
# alimentation 3.3 V ou 5 V
# https://www.seeedstudio.com/Grove-Buzzer.html

import time
from pyb import Pin, Timer

__version__ = (0, 0, 3)

print('Buzzer grove à brancher sur D6')
input('ENTER pour continuer...')

# buzzer sur la sortie D6
# pwm pour réglage du volume
D6 = Pin('D6')
timer2 = Timer(2, freq=2300)  # 2300 Hz fréquence de résonance du buzzer
buzzer = timer2.channel(3, Timer.PWM, pin=D6)  # objet TimerChannel
# buzzer off (volume 0 % duty cycle 0 %)
buzzer.pulse_width_percent(0)

# Remarque : avec un rapport cyclique de 100 %
# le signal pwm est continu (Vcc)
# le buzzer fonctionne alors à sa fréquence propre
# (a priori, son à la fréquence de résonance 2300 Hz)

# Avec un rapport cyclique < 100 %
# on impose un signal pwm de fréquence 2300 Hz (freq du timer2) au buzzer
# (a priori, son de fréquence 2300 Hz)

for volume in range(0, 110, 10):
    print('volume', volume, '%')
    buzzer.pulse_width_percent(volume)
    time.sleep_ms(1000)

# buzzer off
buzzer.pulse_width_percent(0)
timer2.deinit()
