# (C) Fabrice SINCERE

# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

# test OK avec buzzer grove v1.2 (buzzer commandé par un transistor)
# alimentation 3.3 V ou 5 V
# https://www.seeedstudio.com/Grove-Buzzer.html

import time
from collections import OrderedDict  # dictionnaire ordonné
from pyb import Pin, Timer

__version__ = (0, 0, 2)

print('Buzzer grove à brancher sur D6')
input('ENTER pour continuer...')

# buzzer sur la sortie D6
# pwm pour réglage de la fréquence (et du volume)
D6 = Pin('D6')
timer2 = Timer(2, freq=2300)  # 2300 Hz fréquence de résonance du buzzer
buzzer = timer2.channel(3, Timer.PWM, pin=D6)  # objet TimerChannel
# buzzer off (volume 0 %)
buzzer.pulse_width_percent(0)

notes = OrderedDict()  # MicroPython 1.17 est une implémentation de python 3.4
# notes = dict()  # un dictionnaire suffirait avec python >= 3.6
# notes dans l'ordre
notes['do3'] = 262
notes['re3'] = 294
notes['mi3'] = 330
notes['fa3'] = 349
notes['sol3'] = 392
notes['la3'] = 440
notes['si3'] = 494

# volume 50 % (pwm duty cycle 50 %)
# !! volume strictement inférieur à 100 % !!
buzzer.pulse_width_percent(50)

for note, frequence in notes.items():
    print(note, frequence, 'Hz')
    timer2 = Timer(2, freq=frequence)
    time.sleep_ms(1000)

# buzzer off
buzzer.pulse_width_percent(0)
timer2.deinit()
