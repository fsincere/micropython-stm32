# (C) Fabrice SINCERE
# Télémètre à ultrasons

# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG
# test OK

import time
from pyb import Pin
from machine import time_pulse_us

__version__ = (0, 0, 2)

'''
module Grove émetteur-récepteur à ultrasons
-------------------------------------------
version : Ultrasonic distance sensor v2.0

portée (théorique) : 5 mètres

3 broches utiles (connecteur 4 broches) :
shield grove avec commutateur Vcc sur 3.3 V

Vcc -> +3.3 V
GND
Broche 'SIG' (Trigger input/ Echo output) reliée à D4 sur la carte stm32

https://wiki.seeedstudio.com/Grove-Ultrasonic_Ranger/

ou :
module émetteur-récepteur à ultrasons SRF05
-------------------------------------------
portée (théorique) : 4 mètres

4 broches utiles (connecteur 5 broches) :
Vcc -> +5 V (3.3 V pose problème à faible distance)
GND
Broche 'Mode' reliée à la masse (time diagram mode 2)
Broche 'Trigger input/ Echo output' reliée à D4 sur la carte stm32
'''

# vitesse des ultrasons dans l'air
vitesse = 340.0  # en m/s

# module à ultrasons
# configuration en sortie
trigger = Pin('D4', mode=Pin.OUT)
trigger.off()

print("\nCTRL+C pour quitter")
input("ENTER pour continuer")

try:
    while True:
        # Trigger input (côté module)
        # impulsion de 10 µs minimum
        trigger.on()
        time.sleep_us(10)  # temporisation inutile ?
        trigger.off()

        # echo output (côté module)
        # mesure de la durée de propagation
        # configuration en entrée
        echo = Pin('D4', mode=Pin.IN)
        # durée en µs, high pulse
        duree = time_pulse_us(echo, 1)
        distance = int(duree*1e-4*vitesse/2)  # en cm
        # print('dt=', duree, 'µs', 'D=', distance, 'cm')
        print('D=', distance, 'cm')  # Thonny : View -> Plotter

        # Trigger input (côté module)
        trigger = Pin('D4', mode=Pin.OUT)
        trigger.off()

        # pause
        time.sleep_ms(100)
except KeyboardInterrupt:
    print("\nbye!")
