# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG
# F.Sincère

import math
from pyb import DAC, Timer
from array import array
import micropython
micropython.alloc_emergency_exception_buf(100)

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

print("""Un sinus de fréquence f, de valeur moyenne 1,65 V,
de valeur efficace 1 V (composante alternative)
est disponible en sortie du dac (broche A2 ou D13, Cf. code).

Résultats du test :
-----------------

DAC(1)  # 12 bits sortie A2
pour des fréquences de 2 à 300 kHz :
ça fonctionnne bien jusqu'à 10 MHz

Au delà, la fréquence du sinus est divisée par 2 puis par 3 ...
A 80 MHz, signal plat.

DAC(2)  # 12 bits sortie D13
ça fonctionnne bien jusqu'à 5.7 MHz
Timer(6, freq=5714285, prescaler=0, period=13, mode=UP, div=1)

8 bits :
mêmes résultats

Test avec une seconde carte :
DAC(1) A2  : ça fonctionnne bien jusqu'à 8 MHz !
DAC(2) D13 : ça fonctionnne bien jusqu'à 10 MHz !
""")

# Define hardware
dac = DAC(1)  # sortie A2
# dac = DAC(2)  # sortie D13

# dac.init(bits=8, buffering=True)
dac.init(bits=12, buffering=True)

# timer associé au DAC
tim = Timer(6, freq=1000)


def sine(f, n, period):
    # Emit sinewave on DAC
    # f : sine wave frequency (Hz)
    # n : taille du buffer
    # c'est aussi le nombre d'échantillons par période

    print("samples per period : {}".format(n))

    # sampling frequency
    fs = f*n
    print("fs attendue : {} Hz".format(fs))
    # write
    tim.period(period)
    tim.prescaler(0)
    # read and compare
    fs_tim = tim.freq()
    print("fs effective : {} Hz".format(fs_tim))
    if abs(fs-fs_tim) > fs*1e-5:
        raise ValueError("Erreur inattendue")
    print(tim)  # attention affichage freq arrondie à l'entier le plus proche

    # 12 bits
    # 0 à 4095 ; type code  'H' (unsigned integer 2 bytes)
    # moyenne 1,65 V -> 2048
    # valeur efficace 1 V pour la composante alternative
    # 1,4142 V -> 1755
    buf = array('H', 2048+int(1755*math.sin(2*math.pi*i/n))
                for i in range(n))

    # 8 bits
    # 0 à 255 
    # moyenne 1,65 V -> 128
    # valeur efficace 1 V pour la composante alternative
    # 1,4142 V -> 109
    # buf = bytearray([int(128+109*math.sin(2*math.pi*i/n)) for i in range(n)])

    dac.write_timed(buf, tim, mode=DAC.CIRCULAR)


f = float(input(("fréquence du sinus (Hz) ? ")))

for period in range(29, -1, -1):
    fs = 80000000/(period+1)
    print('fs {} MHz'.format(fs))
    n = int(fs/f + 0.5)
    print("f sine wave : {} Hz  {:+.2f} %".format(fs/n, (fs/n-f)/f*100))
    sine(fs/n, n, period)
    input("Enter to continue\n")

dac.deinit()
