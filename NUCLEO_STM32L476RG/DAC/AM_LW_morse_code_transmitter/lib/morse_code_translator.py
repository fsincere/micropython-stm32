# version MicroPython du module CPython morse_code_translator
# https://pypi.org/project/morse-code-translator2/
# https://github.com/yuziaka/morse_code_translator
# ybondarenko.job@gmail.com

# Adaptation : Fabrice Sincère

__version__ = (0, 0, 2)

morse_dictionary = (
    ("a", ".-"),
    ("b", "-..."),
    ("c", "-.-."),
    ("d", "-.."),
    ("e", "."),
    ("f", "..-."),
    ("g", "--."),
    ("h", "...."),
    ("i", ".."),
    ("j", ".---"),
    ("k", "-.-"),
    ("l", ".-.."),
    ("m", "--"),
    ("n", "-."),
    ("o", "---"),
    ("p", ".--."),
    ("q", "--.-"),
    ("r", ".-."),
    ("s", "..."),
    ("t", "-"),
    ("u", "..-"),
    ("v", "...-"),
    ("w", ".--"),
    ("x", "-..-"),
    ("y", "-.--"),
    ("z", "--.."),
)

lat_to_morse = {lat: morse for lat, morse in morse_dictionary}
morse_to_lat = {morse: lat for lat, morse in morse_dictionary}


def _translate_char(char, dictionary):
    """ char: str, dictionary: Dict) -> str """
    char = char.lower()
    try:
        return dictionary[char]
    except KeyError:
        return ""


def _translate_char_to_morse(char):
    return _translate_char(char=char, dictionary=lat_to_morse)


def _translate_char_to_lat(char):
    return _translate_char(char=char, dictionary=morse_to_lat)


def translate_to_morse(text):
    """Translating Latin letters into Morse code."""

    return " ".join(
        _translate_char_to_morse(char=char) if char != " " else ""
        for char in text
        if char
    ).strip(" ")


def translate_to_lat(text):
    """Translating Morse code into Latin letters."""

    result = "".join(
        _translate_char_to_lat(char=char) if char != "" else " "
        for char in text.split(" ")
    )
    return result.replace("  ", " ")
