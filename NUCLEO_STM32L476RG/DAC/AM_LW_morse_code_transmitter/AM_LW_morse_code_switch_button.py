# Emetteur code Morse en modulation AM (grandes ondes LW)
# pour réception sur radio AM grandes ondes

# Génération d'un signal modulé AM sur la broche A2
# (avec antenne filaire la plus longue possible)
# quand on appuie sur le bouton poussoir user (bleu)
# On peut choisir l'indice de modulation
# (modulation AM analogique avec un modulant sinus)
# (avec m = 0, on obtient une modulation ASK-OOK purement numérique)

# (C) Fabrice Sincère
# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

# version DAC 8 bits
# rappel : le DAC fonctionne correctement avec un sinus pleine échelle
# de fréquence max # 265 kHz (limitation liée au settling time)

# test OK avec autoradio Sony

from pyb import DAC, Pin, ExtInt, LED, Timer
import math
import micropython

__version__ = (0, 0, 6)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

micropython.alloc_emergency_exception_buf(100)


def interrupt_bp(event):
    #  interruption sur le bouton-poussoir
    if bp() == 0:
        # on appuie sur le bouton-poussoir
        led.on()
        dac.write_timed(buf, tim, mode=DAC.CIRCULAR)
    else:
        # on relache le bouton-poussoir
        led.off()
        dac.deinit()


# led built-in
led = LED(1)
led.off()
# bouton-poussoir USER
bp = Pin("C13", mode=Pin.IN)
# bp au repos -> niveau 1
# bp appuyé   -> niveau 0

# interruption
extint = ExtInt(bp, mode=ExtInt.IRQ_RISING_FALLING,
                pull=Pin.PULL_NONE, callback=interrupt_bp)
extint.enable()

print("Emetteur code Morse")
print("Porteuse LW entre 140 et 300 kHz")  # porteuse sinus
fporteuse = 1000*int(input("Fréquence de la porteuse (en kHz) ? "))   # en Hz

# sinus ; pas trop petit pour limiter la taille du buffer
# si buffer trop grand :
# MemoryError: memory allocation failed
fmodulant = 1000.0

# sampling frequency # 5 MHz max
# timer associé au DAC
# horloge interne du cpu (80 MHz)
# sampling frequency = 80 MHz/(period+1)
tim = Timer(6, prescaler=0, period=15)  # 5 MHz avec period=15
fe = tim.freq()
m = 1.0  # indice de modulation

# taille du buffer
n = int(fe/fmodulant)
# correction
fmodulant = fe/n
print("\nFréquence d'echantillonnage {} Hz".format(fe))
print("Fréquence du modulant {} Hz".format(fmodulant))
print("Buffer : {} échantillons".format(n))

# create a buffer
# DAC 8 bits 0 -> 0 V    255 -> 3.3 V
# amplitude 90 %
buf = bytearray([128+int(114/(1+m)*(1+m*math.sin(2*math.pi*fmodulant*i/fe))*math.sin(2*math.pi*fporteuse*i/fe)) for i in range(n)])

dac = DAC(1)  # broche Arduino A2
dac.init(8, buffering=True)
print("""\nSignal AM disponible sur la broche A2
Brancher un oscilloscope et une antenne
""")

print("Appuyer sur le bouton-poussoir 'user' pour générer du code Morse")

input('Enter to quit')

extint.disable()
tim.deinit()
dac.deinit()
del(buf)
