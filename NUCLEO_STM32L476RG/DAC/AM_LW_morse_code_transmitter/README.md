## Carte NUCLEO STM32L476RG

### Emetteur code Morse en modulation d'amplitude AM dans la bande LW (GO)

#### Script MicroPython : AM_LW_morse_code_switch_button.py

```
>>>
Emetteur code Morse
Porteuse LW entre 140 et 300 kHz
Fréquence de la porteuse (en kHz) ? 200

Fréquence d'echantillonnage 5000000 Hz
Fréquence du modulant 1000.0 Hz
Buffer : 5000 échantillons

Signal AM disponible sur la broche A2
Brancher un oscilloscope et une antenne

Appuyer sur le bouton-poussoir 'user' pour générer du code Morse
Enter to quit
>>> 
```

#### Script MicroPython : AM_LW_morse_code_translator.py

Le module *morse_code_translator.py* est nécessaire.  
 
```
>>>
Emetteur code Morse
Porteuse LW entre 140 et 300 kHz
Fréquence de la porteuse (en kHz) ? 200

Fréquence d'echantillonnage 5000000 Hz
Fréquence du modulant 1000.0 Hz
Buffer : 5000 échantillons

Signal AM disponible sur la broche A2
Brancher un oscilloscope et une antenne

Le message à transmettre ne doit contenir que des lettres et des espaces
Enter pour quitter

Message à transmettre ? SOS Titanic We have struck an ice berg
Message transmis : sos titanic we have struck an ice berg
. . .    - - -    . . .       -    . .    -    . -    - .    . .    - . - .       . - -    .       . . . .    . -    . . . -    .       . . .    -    . - .    . . -    - . - .    - . -       . -    - .       . .    - . - .    .       - . . .    .    . - .    - - . 

>>>
```

#### Script MicroPython : AM_LW_morse_code_translator_and_switch_button.py

Le module *morse_code_translator.py* est nécessaire.  
Par défaut, le manipulateur Morse est le bouton-poussoir USER.  
Vous pouvez également connecter un bouton-poussoir externe (Cf. code).  

```
>>>
Emetteur code Morse
Porteuse LW entre 140 et 300 kHz
Fréquence de la porteuse (en kHz) ? 153

Fréquence d'echantillonnage 5000000 Hz
Fréquence du modulant 1000.0 Hz
Buffer : 5000 échantillons

Signal AM disponible sur la broche A2
Brancher un oscilloscope et une antenne

Appuyer sur le bouton-poussoir 'user' pour générer du code Morse
ou saisir le message à transmettre (ne doit contenir que des lettres et des espaces)
Enter pour quitter

Message à transmettre ?
>>>
```

(C) Fabrice Sincère