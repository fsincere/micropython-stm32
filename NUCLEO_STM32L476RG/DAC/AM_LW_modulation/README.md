## Carte NUCLEO STM32L476RG

### Modulation d'amplitude AM dans la bande LW (GO)

Le signal modulant et la porteuse sont de forme sinusoïdale.  
La fréquence max. de la porteuse est de l'ordre de 300 kHz (environ 17 échantillons par période avec une fréquence d'échantillonnage de 5 MHz).  
Il faut limiter la taille du buffer à 5000 ou 10000 échantillons (au delà : MemoryError: memory allocation failed).  
Pour des fréquences du modulant plus petites, il faudra donc réduire la fréquence d'échantillonnage.  
Par exemple, avec un modulant 200 Hz et une porteuse de 8 kHz, on pourra choisir une fréquence d'échantillonnage de 1 MHz (`tim = Timer(6, prescaler=0, period=79)`).  

Dans la bande LW, on peut brancher une antenne filaire en sortie A2 pour faire un émetteur radio (de très faible puissance).  


#### Script MicroPython : AM_LW_switch_button.py

```
>>>
Porteuse LW entre 140 et 300 kHz
Fréquence de la porteuse (en kHz) ? 200
Indice de modulation (en %) ? 50

Fréquence d'echantillonnage 5000000 Hz
Fréquence du modulant 1000.0 Hz
Buffer : 5000 échantillons

Signal AM disponible sur la broche A2
Brancher un oscilloscope et une antenne

Appuyer sur le bouton-poussoir 'user' pour générer le signal AM
Enter to quit
>>> 
```

- indice de modulation 50 % (modulant sinusoïdal)

![AM modulation](images/am_modulation1.png)

- indice de modulation 100 % (modulant sinusoïdal)

![AM modulation](images/am_modulation2.png)

(C) Fabrice Sincère
