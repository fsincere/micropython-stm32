# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG
# (C) Fabrice Sincère

"""Générateur signal sinus avec bruit blanc (bruit gaussien)
Valeur moyenne 1.65 V
On peut utiliser le mode de couplage AC de l'oscilloscope
pour supprimer la composante continue.
Sortie sur la broche A2.
Test multimètre, oscilloscope : OK
Remarque : pour le spectre, l'effet de l'échantillonnage du DAC se fait sentir
=> il faut se limiter à la plage 0 à fmax = fe/2
"""

import micropython
import uos  # urandom
import math
from array import array
from pyb import DAC
micropython.alloc_emergency_exception_buf(100)

__version__ = (0, 0, 5)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

print("Générateur signal sinus avec bruit blanc")

f = 3000
print("Fréquence du signal sinus : {} Hz".format(f))

# nombre de période du signal sinus
k = 20  # 20 c'est bien
# nombre d'échantillons par période
ns = 200
# nombre d'échantillons
n = k*ns  # 4000 points c'est bien
print(n, "échantillons")
# fréquence d'échantillonnage
fe = f*ns  # 600 kHz max en pleine échelle
print("Fréquence d'échantillonnage : {} Hz".format(fe))

print("Bande passante du bruit blanc fmax {} Hz".format(fe/2))

srms_max = 1000  # mV (1.65/racine(2) = 1.1667 V)
srms_min = 0  # seulement du bruit

while True:
    try:
        srms = float(input("Valeur efficace du signal sinus (en mV) ? "))
        if srms_min <= srms <= srms_max:
            break
        else:
            print("Valeur attendue entre {} et {} mV"
                  .format(srms_min, srms_max))
    except ValueError:
        print("Valeur invalide")

# valeur moyenne 1,65 V (Vcc/2)
# 295.6/4095*3.3 = 238.2 mVeff (1 sigma) pour l'intervalle complet 0-4095
# en pratique on se limite à 4 sigmas (6 dépassements pour 100000)

brms_max = (1166.7-srms)*2048/1166.7/4
brms_min = 0  # pas de bruit (mais il y aura du bruit de quantification)

while True:
    try:
        brms = float(input("Valeur efficace du bruit (en mV) ? "))
        if brms_min <= brms <= brms_max:
            break
        else:
            print("Valeur attendue entre {} et {} mV"
                  .format(brms_min, brms_max))
    except ValueError:
        print("Valeur invalide")

# rapport signal sur bruit
try:
    snr = srms/brms
    snrdb = 20*math.log10(snr)
    print("Rapport signal sur bruit : {} dB".format(snrdb))
except ValueError:
    pass

# create a buffer (dac 12 bits)
# 0 -> 0V
# 2048 -> 1.65 V
# 4095 -> 3.3 V
# loi normale approximée par la somme de 16 octets aléatoires d'une loi uniforme
# (Théorème central limite)
buf = array('H', 2048+int(srms*math.sqrt(2)*4096/3300*math.sin(2*math.pi*f*i/fe)
            + (sum(uos.urandom(16))-2040)*brms/238.2+0.5) for i in range(n))

# dac = DAC(2) # broche Arduino D13
dac = DAC(1)  # broche A2
# dac.init(12, buffering=False)
dac.init(12, buffering=True)
dac.write(0)  # 0 V

input('Enter pour générer le bruit blanc (sortie A2)')
dac.write_timed(buf, fe, mode=DAC.CIRCULAR)  # output buffer enabled

input('Enter pour quitter le programme')
dac.deinit()
del(buf)

"""Exemple de rendu :
>>>
Générateur signal sinus avec bruit blanc
Fréquence du signal sinus : 3000 Hz
4000 échantillons
Fréquence d'échantillonnage : 600000 Hz
Bande passante du bruit blanc fmax 300000.0 Hz
Valeur efficace du signal sinus (en mV) ? 500
Valeur efficace du bruit (en mV) ? 50
Rapport signal sur bruit : 20.0 dB
Enter pour générer le bruit blanc (sortie A2)
Enter pour quitter le programme
>>>
"""
