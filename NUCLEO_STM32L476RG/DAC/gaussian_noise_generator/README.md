## Carte NUCLEO STM32L476RG

### Générateur de bruit blanc (bruit gaussien)

Script MicroPython : gaussian_noise_generator.py

```
>>>
Générateur de bruit blanc
Bande passante du bruit blanc 300000.0 Hz
Valeur efficace du bruit (en mV) ? 100
Enter pour générer le bruit blanc (sortie A2)
Enter pour quitter le programme
>>>
```

![gaussian noise](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/DAC/gaussian_noise_generator/images/gaussian_noise.png)


### Sinus avec du bruit blanc

Script MicroPython : sine_with_gaussian_noise_generator.py

```
>>>
Générateur signal sinus avec bruit blanc
Fréquence du signal sinus : 3000 Hz
4000 échantillons
Fréquence d'échantillonnage : 600000 Hz
Bande passante du bruit blanc fmax 300000.0 Hz
Valeur efficace du signal sinus (en mV) ? 500
Valeur efficace du bruit (en mV) ? 50
Rapport signal sur bruit : 20.0 dB
Enter pour générer le bruit blanc (sortie A2)
Enter pour quitter le programme
>>>
```

![sine wave with gaussian noise](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/DAC/gaussian_noise_generator/images/sine_wave_with_gaussian_noise.png)


![sine wave with gaussian noise 1024 samples FFT spectrum](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/DAC/gaussian_noise_generator/images/sine_wave_with_gaussian_noise_spectrum.png)



