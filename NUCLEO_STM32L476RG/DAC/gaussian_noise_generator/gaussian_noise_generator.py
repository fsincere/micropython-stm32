# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG
# (C) Fabrice Sincère

"""Générateur de bruit blanc (bruit gaussien)
Valeur moyenne 1.65 V
On peut utiliser le mode de couplage AC de l'oscilloscope
pour supprimer la composante continue.
Sortie sur la broche A2.
Test multimètre, oscilloscope : OK
Remarque : pour le spectre, l'effet de l'échantillonnage du DAC se fait sentir
=> il faut se limiter à la plage 0 à fmax = fe/2
"""

import micropython
import uos  # urandom
from array import array
from pyb import DAC
micropython.alloc_emergency_exception_buf(100)

__version__ = (0, 0, 6)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

print("Générateur de bruit blanc")

# fréquence échantillonnage
# 600 kHz max en pleine échelle (settling time 1,67 µs)
fe = 600000
print("Bande passante du bruit blanc {} Hz".format(fe/2))

# valeur moyenne 1,65 V (Vcc/2)
# 295.6/4095*3.3 = 238.2 mVeff pour l'intervalle complet 0-4095
# 2048/295.6 = 6,9 écart-type
# en prenant 500 mVeff, cela donne 238.1/500*6.9 = 3.3 sigmas,
# soit 1 dépassement pour 1000
# avec 4000 échantillons, cela fait 4 dépassements

rms_max = 500  # en pratique 500 mV sans risque génant de dépassement
rms_min = 2  # doit être beaucoup plus grand que le bruit de quantification:
# écart-type loi uniforme : 3.3/4096/racine(12) = 0.23 mV avec DAC 12 bits

while True:
    try:
        rms = float(input("Valeur efficace du bruit (en mV) ? "))
        if rms_min <= rms <= rms_max:
            break
        else:
            print("Valeur attendue entre {} et {} mV".format(rms_min, rms_max))
    except ValueError:
        print("Valeur invalide")

# nombre d'échantillons
n = 4000  # 4000 c'est bien

# create a buffer (dac 12 bits)
# 0 -> 0V
# 2048 -> 1.65 V
# 4095 -> 3.3 V
# loi normale approximée par la somme de 16 octets aléatoires d'une loi uniforme
# (Théorème central limite)
buf = array('H', 2048+int((sum(uos.urandom(16))-2040)*rms/238.2+0.5)
            for i in range(n))

# dac = DAC(2) # broche Arduino D13
dac = DAC(1)  # broche A2
# dac.init(12, buffering=False)
dac.init(12, buffering=True)
dac.write(0)  # 0 V

input('Enter pour générer le bruit blanc (sortie A2)')
dac.write_timed(buf, fe, mode=DAC.CIRCULAR)  # output buffer enabled

input('Enter pour quitter le programme')
dac.deinit()
del(buf)

"""Exemple de rendu :
>>>
Générateur de bruit blanc
Bande passante du bruit blanc 300000.0 Hz
Valeur efficace du bruit (en mV) ? 100
Enter pour générer le bruit blanc (sortie A2)
Enter pour quitter le programme
>>>
"""
