## Carte NUCLEO STM32L476RG

Cette carte possède 2 convertisseurs numérique-analogique (DAC Digital to analog converter) :

- DAC(1) en sortie A2
- DAC(2) en sortie D13 (avec led built-in)

Plage : 0 à 3.3 V

Résolution : 8 ou 12 bits

Fréquence d'échantillonnage max : 5 MHz (et jusqu'à 10 MHz)

Settling time : environ 2 V/µs

(ce qui autorise la génération d'un sinus en pleine échelle
de fréquence max. environ 250 kHz)

Attention :

- non linéarité des convertisseurs dans la plage 0 à 50 mV (défaut du circuit buffer ?)
- avec la sortie D13, saturation haute à 3,1 V (influence de la LED built-in ?)


### Utilisation basique

```python
MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

>>> from pyb import DAC

>>> dac = DAC(2)  # sortie D13
>>> # dac = DAC(1)  # sortie A2
>>> dac.init(bits=8, buffering=True)
>>> # dac.init(bits=12, buffering=True)
>>> buffer = bytearray(b'\x00\xff')
>>> fs = 10  # sampling frequency (Hz)
>>> # on génère un créneau 0/3.3V de fréquence 5 Hz
>>> # la led built-in clignote
>>> dac.write_timed(buffer, fs, mode=DAC.CIRCULAR)
>>> help(dac)
object DAC(2, bits=8) is of type DAC
  init -- <function>
  deinit -- <function>
  write -- <function>
  noise -- <function>
  triangle -- <function>
  write_timed -- <function>
  NORMAL -- 0
  CIRCULAR -- 32
>>> dac.deinit()
```

N.B. La fréquence d'échantillonnage est ici de type *int*.   
Voir ci-dessous pour gérer le type *float*.


### Utilisation avec le timer associé

En interne, les DAC sont cadencés avec un timer (par défaut le timer 6).

```python
>>> from pyb import DAC, Timer

>>> tim = Timer(6, freq=10)
>>> dac = DAC(2)  # sortie D13
>>> dac.init(bits=8, buffering=True)
>>> buffer = bytearray(b'\x00\xff')
>>> # on génère un créneau 0/3.3V de fréquence 5 Hz
>>> # la led built-in clignote
>>> dac.write_timed(buffer, tim, mode=DAC.CIRCULAR)
>>> tim.freq(2.5)  # float
>>> dac.deinit()
```

### Remarques

C'est l'horloge interne du cpu (80 MHz) qui cadence les timers, et donc les DACs.  
Le constructeur indique une tolérance de 0.25 %.  

```python
>>> pyb.freq()
(80000000, 80000000, 80000000, 80000000)
```

Pour obtenir 10 Hz, la fréquence de 80 MHz est divisée par un nombre entier qui dépend du choix des registres *prescaler* et *period* :  

```python
>>> tim = Timer(6, freq=10)
>>> tim
Timer(6, freq=10, prescaler=124, period=63999, mode=UP, div=1)
```

80 MHz/(124+1)/(63999+1) = 10 Hz exactement (mais avec une tolérance de 0.25 %).  

**Attention** : certaines fréquences d'échantillonnage ne sont pas accessibles.  

```python
>>> tim = Timer(6, freq=3000000)
>>> tim
Timer(6, freq=3076923, prescaler=0, period=25, mode=UP, div=1)
>>> tim.freq()
3076923.0
```
Avec fs = 3 MHz, nous aurons en réalité 3.076923 MHz (c'est-à-dire 80 MHz/(25+1)).  


