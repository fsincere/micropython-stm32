# (C) Fabrice SINCERE
# Générateur DTMF
# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

# test : OK
import math
import pyb  # DAC, (Timer)

__version__ = (0, 0, 3)

print("""
Generates a DTMF waveform on pin A2.
""")


def codage_dtmf(touche):
    # DTMF
    '''
       1209 Hz 1336 Hz 1477 Hz
697 Hz   1       2       3
770 Hz   4       5       6
852 Hz   7       8       9
941 Hz   *       0       #
retourne f1, f2
retourne None si la touche est invalide
'''
    touche = str(touche)
    if touche == '1':
        f1, f2 = 697, 1209
    elif touche == '2':
        f1, f2 = 697, 1336
    elif touche == '3':
        f1, f2 = 697, 1477
    elif touche == '4':
        f1, f2 = 770, 1209
    elif touche == '5':
        f1, f2 = 770, 1336
    elif touche == '6':
        f1, f2 = 770, 1477
    elif touche == '7':
        f1, f2 = 852, 1209
    elif touche == '8':
        f1, f2 = 852, 1336
    elif touche == '9':
        f1, f2 = 852, 1477
    elif touche == '*':
        f1, f2 = 941, 1209
    elif touche == '0':
        f1, f2 = 941, 1336
    elif touche == '#':
        f1, f2 = 941, 1477
    else:
        return None
    return f1, f2


# DAC
fe = 50000
Ne = 5000  # nombre d'échantillons
# durée du cycle circulaire : Ne/fe
print("Durée du cycle DTMF : {} s".format(Ne/fe))

dac = pyb.DAC(1)  # sortie A2
# dac = pyb.DAC(2)  # sortie D13
dac.init(bits=8, buffering=True)

print("\nCTRL+C pour quitter")
try:
    while True:
        while True:
            touche_ = input("\nTouche ? ")
            res = codage_dtmf(touche=touche_)
            if res is None:
                print("Touche invalide")
            else:
                f1, f2 = res
                break

        print('Touche {} [{} {} Hz]'.format(touche_, f1, f2))

        # 8 bits
        outputsamples = bytearray(128+int(63*math.cos(2*math.pi*f1*x/fe)
                                  + 63*math.cos(2*math.pi*f2*x/fe)) for x in range(Ne))

        # dac : timer6 par défaut
        dac.write_timed(outputsamples, fe, mode=pyb.DAC.CIRCULAR)

        # dac : on peut utiliser un autre timer (timer4)
        # tim = pyb.Timer(4)
        # tim.init(freq=fe)
        # dac.write_timed(outputsamples, tim, mode=pyb.DAC.CIRCULAR)
except KeyboardInterrupt:
    print('\nbye !')
    dac.deinit()
