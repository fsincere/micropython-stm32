## Carte NUCLEO STM32L476RG

### Générateur de code DTMF (dual-tone multi-frequency)

Script MicroPython : generateur_dac_DTMF.py

```
>>>
Generates a DTMF waveform on pin A2.
Durée du cycle DTMF : 0.1 s
CTRL+C pour quitter

Touche ? 1
Touche 1 [697 1209 Hz]
>>>
```

![DTMF signal key 1](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/DAC/DTMF_generator/images/DTMFsignal_key1.png)

![DTMF signal key 1 spectrum](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/DAC/DTMF_generator/images/DTMFsignal_key1_spectrum.png)
