## Carte NUCLEO STM32L476RG

### Générateur de tension sinusoïdale 10 mHz à 300 kHz

#### Script MicroPython : generateur_sinus.py

```
>>>
Un sinus de fréquence f, de valeur moyenne 1,65 V,
de valeur efficace 1 V (composante alternative)
est disponible en sortie du dac (broche A2 ou D13).

CPU frequency : 80.0 MHz
Max sampling frequency : 5.0 MHz
CTRL+C pour quitter

Fréquence du signal sinus (Hz) ? 0.01
2000 échantillons par période ; fs : 20 Hz ; f : 0.01 Hz ; écart +0.00 %
[OK]
Fréquence du signal sinus (Hz) ? 2.5
2000 échantillons par période ; fs : 5000 Hz ; f : 2.5 Hz ; écart +0.00 %
[OK]
Fréquence du signal sinus (Hz) ? 100000
50 échantillons par période ; fs : 5000000 Hz ; f : 100000.0 Hz ; écart +0.00 %
[OK]
Fréquence du signal sinus (Hz) ? 225000
Warning : Précision dégradée
21 échantillons par période ; fs : 4705882.0 Hz ; f : 224089.6 Hz ; écart -0.40 %
[OK]
>>>
```

- 2,5 kHz

![sine wave](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/DAC/sine_generator/images/sine_wave_400ms.png)

- 100 kHz

![sine wave](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/DAC/sine_generator/images/sine_wave_100kHz.png)


- 225 kHz : un peu de distorsion

![sine wave](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/DAC/sine_generator/images/sine_wave_225kHz.png)

