# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

# F.Sincère
# test : oscillo OK

import math
from pyb import DAC, Timer
from array import array
import micropython
micropython.alloc_emergency_exception_buf(100)

__version__ = (0, 0, 8)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

print("""Un sinus de fréquence f, de valeur moyenne 1,65 V,
de valeur efficace 1 V (composante alternative)
est disponible en sortie du dac (broche A2 ou D13).
""")

# Define hardware
# dac = DAC(1)  # sortie A2
dac = DAC(2)  # sortie D13
dac.init(bits=12, buffering=True)

# cpu frequency ; tolérance 0.25 % d'après constructeur
CPU_freq = pyb.freq()[0]  # (80000000, 80000000, 80000000, 80000000)
print("CPU frequency : {} MHz".format(CPU_freq*1e-6))

PERIOD = 15
# 5 MHz PERIOD = 15 ;  5.333333 MHz PERIOD = 14 ; 10 MHz PERIOD = 7
FSmax = CPU_freq/(PERIOD+1)
print("Max sampling frequency : {} MHz".format(FSmax*1e-6))

# timer associé au DAC
tim = Timer(6, freq=FSmax)
# print(tim)

Fsine_max = 300000  # Hz
Fsine_min = 0.01  # Hz

N_min = 16	 # min samples per period
N_max = 2000

Ecart_max = 0.05  # %

# amplitude crête à crête (en V)
Vpp = 2*2**0.5  # Vrms ac = 1 V


def get_fs(fs):
    """ fs est la fréquence d'échantillonage désirée (en Hz).
    retourne la fréquence d'échantillonage la plus proche
    que propose le timer
    (fs, écart en %, prescaler, period)
    >>> get_fs(3210000)
    (3200000, -0.3115265, 0, 24)
    """
    # fs fréquence d'échantillonage désirée
    tim.freq(fs)
    # fréquence d'échantillonage choisie par le timer
    _fs = tim.freq()
    ecart = (_fs-fs)/fs*100  # écart en %
    period = tim.period()
    prescaler = tim.prescaler()

    # la fréquence proposée par le timer n'est pas forcement la plus proche
    # par exemple :
    # 3210000 Hz -> (3333333.0, 1.010098 %, 0, 23)
    # alors qu'avec period = 24, on a 3200000
    # (3200000, -0.3115265, 0, 24)

    prescaler2 = prescaler
    period2 = period+1
    if period2 == 65536:
        period2 = int(65536*(prescaler+1)/(prescaler+2))+1
        prescaler2 = prescaler+1
    tim.period(period2)
    tim.prescaler(prescaler2)
    _fs2 = tim.freq()
    ecart2 = (_fs2-fs)/fs*100  # écart en %

    # on retourne la valeur la plus proche
    if abs(ecart) <= abs(ecart2):
        return _fs, ecart, prescaler, period
    return _fs2, ecart2, prescaler2, period2


def sine(f):
    # Emit sinewave on DAC
    # 300 kHz max à cause du settling time d'environ 2 µs
    # test oscillo : OK (sinus légèrement déformé à partir de 150 kHz)

    if f == 0:
        # 1,65 V DC
        dac.write(2048)
    elif not Fsine_min <= f <= Fsine_max:
        raise ValueError("La fréquence doit être comprise entre {} et {} Hz"
                         .format(Fsine_min, Fsine_max))
    else:
        # taille_buffer_max
        # fs = 5 MHz f sine 240 kHz => 20.83333 =>  21
        t = int(FSmax/f + 0.5)  # idéalement entier pour un écart nul

        if t < N_min:
            raise ValueError("La fréquence est trop élevée")
        if t > N_max:
            # f sine max = FSmax/N_max = 5e6/2000 = 2500 Hz
            t = N_max

        # print("max {} échantillons par période".format(t))

        ecart_min = None
        period_min = None
        prescaler_min = None
        n_min = None
        fs_min = None
        f_min = None

        for n in range(t, N_min-1, -1):  # samples per period
            # fréquence d'échantillonage désirée : n*f
            # fréquence d'échantillonage choisie par le timer :
            (fs, ecart, prescaler, period) = get_fs(n*f)
            # print("{} échantillons par période ; fs : {} Hz ; f : {} Hz ; écart {:+.2f} %"
            #       .format(n, fs, fs/n, ecart))

            if ecart_min is None:
                ecart_min = ecart
                period_min = period
                prescaler_min = prescaler
                n_min = n
                fs_min = fs
                f_min = fs/n
            elif abs(ecart) < abs(ecart_min):
                ecart_min = ecart
                period_min = period
                prescaler_min = prescaler
                n_min = n
                fs_min = fs
                f_min = fs/n
            if abs(ecart) < Ecart_max:  # %
                break
        else:
            # pas de break
            # ecart > Ecart_max
            print("Warning : Précision dégradée")

        print("{} échantillons par période ; fs : {} Hz ; f : {} Hz ; écart {:+.2f} %"
              .format(n_min, fs_min, f_min, ecart_min))
        tim = Timer(6, prescaler=prescaler_min, period=period_min)
        # print(tim)
        if abs((tim.freq()/n_min-f_min)) > f_min*Ecart_max*0.01:  # verification
            raise ValueError("Erreur inattendue")

        # 0 à 4095 ; type code  'H' (unsigned integer 2 bytes)
        # moyenne 1,65 V -> 2048
        # Vpp 2.828427 V -> 620.5
        buf = array('H', 2048+int(620.5*Vpp*math.sin(2*math.pi*i/n_min))
                    for i in range(n_min))
        dac.write_timed(buf, tim, mode=DAC.CIRCULAR)


print("CTRL+C pour quitter\n")

try:
    while True:
        try:
            f = float(input("Fréquence du signal sinus (Hz) ? "))
            sine(f)
            print("[OK]")
        except ValueError as e:
            print(e)
except KeyboardInterrupt:
    print("\nBye !")

dac.deinit()
