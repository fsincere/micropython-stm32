# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

# (C) Fabrice Sincère

import pyb
import math
import random

PI = math.pi

__version__ = (0, 0, 6)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

print(""" Emulation d'un diagramme de constellation

Matériel : 1 carte NUCLEO-L476RG et un oscilloscope

sortie A2  --> I (In-phase)
sortie D13 --> Q (Quadrature)

A2  à brancher sur la voie 1 de l'oscilloscope
D13 à brancher sur la voie 2 de l'oscilloscope

Oscilloscope en mode X-Y
et utilisation de la persistance.

B-PSK   --> 0
Q-PSK   --> 1
8-PSK   --> 2
16-PSK  --> 3
4-ASK   --> 4
8-ASK   --> 5
16-ASK  --> 6
16-QAM  --> 7
32-QAM  --> 8
64-QAM  --> 9
""")

while True:
    choix = input('Votre choix ? ')
    if choix in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]:
        choix = int(choix)
        break
    print("Choix invalide")

if choix == 0:
    # B-PSK
    # I, Q
    symbol = [[-1, 0], [1, 0]]
    Imin, Imax = -3, 3  # 0, 255
    Qmin, Qmax = -3, 3  # 0, 255

elif choix == 1:
    # Q-PSK
    # I, Q
    symbol = [[-1, -1], [-1, 1], [1, -1], [1, 1]]
    Imin, Imax = -3, 3  # 0, 255
    Qmin, Qmax = -3, 3  # 0, 255

elif choix == 2:
    # 8-PSK
    # I, Q
    symbol = [[math.cos(PI/8), math.sin(PI/8)],
              [math.cos(3*PI/8), math.sin(3*PI/8)],
              [math.cos(5*PI/8), math.sin(5*PI/8)],
              [math.cos(7*PI/8), math.sin(7*PI/8)],
              [math.cos(-PI/8), math.sin(-PI/8)],
              [math.cos(-3*PI/8), math.sin(-3*PI/8)],
              [math.cos(-5*PI/8), math.sin(-5*PI/8)],
              [math.cos(-7*PI/8), math.sin(-7*PI/8)]]
    Imin, Imax = -2, 2  # 0, 255
    Qmin, Qmax = -2, 2  # 0, 255

elif choix == 3:
    # 16-PSK
    # I, Q
    symbol = [[math.cos(PI/16), math.sin(PI/16)],
              [math.cos(3*PI/16), math.sin(3*PI/16)],
              [math.cos(5*PI/16), math.sin(5*PI/16)],
              [math.cos(7*PI/16), math.sin(7*PI/16)],
              [math.cos(9*PI/16), math.sin(9*PI/16)],
              [math.cos(11*PI/16), math.sin(11*PI/16)],
              [math.cos(13*PI/16), math.sin(13*PI/16)],
              [math.cos(15*PI/16), math.sin(15*PI/16)],
              [math.cos(-PI/16), math.sin(-PI/16)],
              [math.cos(-3*PI/16), math.sin(-3*PI/16)],
              [math.cos(-5*PI/16), math.sin(-5*PI/16)],
              [math.cos(-7*PI/16), math.sin(-7*PI/16)],
              [math.cos(-9*PI/16), math.sin(-9*PI/16)],
              [math.cos(-11*PI/16), math.sin(-11*PI/16)],
              [math.cos(-13*PI/16), math.sin(-13*PI/16)],
              [math.cos(-15*PI/16), math.sin(-15*PI/16)]]
    Imin, Imax = -2, 2  # 0, 255
    Qmin, Qmax = -2, 2  # 0, 255

elif choix == 4:
    # 4-ASK
    # I, Q
    symbol = [[-3, 0], [-1, 0], [1, 0], [3, 0]]
    Imin, Imax = -5, 5  # 0, 255
    Qmin, Qmax = -5, 5  # 0, 255

elif choix == 5:
    # 8-ASK
    # I, Q
    symbol = [[-7, 0], [-5, 0], [-3, 0], [-1, 0],
              [1, 0], [3, 0], [5, 0], [7, 0]]
    Imin, Imax = -9, 9  # 0, 255
    Qmin, Qmax = -9, 9  # 0, 255

elif choix == 6:
    # 16-ASK
    # I, Q
    symbol = [[-15, 0], [-13, 0], [-11, 0], [-9, 0],
              [-7, 0], [-5, 0], [-3, 0], [-1, 0],
              [1, 0], [3, 0], [5, 0], [7, 0],
              [9, 0], [11, 0], [13, 0], [15, 0]]
    Imin, Imax = -17, 17  # 0, 255
    Qmin, Qmax = -17, 17  # 0, 255


elif choix == 7:
    # 16-QAM
    # I, Q
    symbol = [[-3, -3], [-3, -1], [-3, 1], [-3, 3],
              [-1, -3], [-1, -1], [-1, 1], [-1, 3],
              [1, -3], [1, -1], [1, 1], [1, 3],
              [3, -3], [3, -1], [3, 1], [3, 3]]
    Imin, Imax = -5, 5  # 0, 255
    Qmin, Qmax = -5, 5  # 0, 255

elif choix == 8:
    # 32-QAM
    # I, Q
    symbol = [[-3, -3], [-3, -1], [-3, 1], [-3, 3],
              [-1, -3], [-1, -1], [-1, 1], [-1, 3],
              [1, -3], [1, -1], [1, 1], [1, 3],
              [3, -3], [3, -1], [3, 1], [3, 3],
              [-5, -3], [-5, -1], [-5, 1], [-5, 3],
              [5, -3], [5, -1], [5, 1], [5, 3],
              [-3, 5], [-1, 5], [1, 5], [3, 5],
              [-3, -5], [-1, -5], [1, -5], [3, -5]]
    Imin, Imax = -7, 7  # 0, 255
    Qmin, Qmax = -7, 7  # 0, 255

elif choix == 9:
    # 64-QAM
    # I, Q
    symbol = [[-7, -7], [-5, -7], [-3, -7], [-1, -7],
              [1, -7], [3, -7], [5, -7], [7, -7],
              [-7, -5], [-5, -5], [-3, -5], [-1, -5],
              [1, -5], [3, -5], [5, -5], [7, -5],
              [-7, -3], [-5, -3], [-3, -3], [-1, -3],
              [1, -3], [3, -3], [5, -3], [7, -3],
              [-7, -1], [-5, -1], [-3, -1], [-1, -1],
              [1, -1], [3, -1], [5, -1], [7, -1],
              [-7, 1], [-5, 1], [-3, 1], [-1, 1],
              [1, 1], [3, 1], [5, 1], [7, 1],
              [-7, 3], [-5, 3], [-3, 3], [-1, 3],
              [1, 3], [3, 3], [5, 3], [7, 3],
              [-7, 5], [-5, 5], [-3, 5], [-1, 5],
              [1, 5], [3, 5], [5, 5], [7, 5],
              [-7, 7], [-5, 7], [-3, 7], [-1, 7],
              [1, 7], [3, 7], [5, 7], [7, 7]]
    Imin, Imax = -9, 9  # 0, 255
    Qmin, Qmax = -9, 9  # 0, 255

# mise à l'échelle
# DAC 8 bits 0 -> 0 V    255 -> 3.3 V

symbol_dac = []

for val in symbol:
    i, q = val
    i_dac = int((i-Imin)*255/(Imax-Imin))
    q_dac = int((q-Qmin)*255/(Qmax-Qmin))

    symbol_dac.append([i_dac, q_dac])

# create a buffer
# nombre d'échantillons
# buffer size
samples = 2000

# sampling frequency Hz
# symbol rate (baud)
fs = 1250  # int

bufI = bytearray()
bufQ = bytearray()

for j in range(samples):
    i, q = random.choice(symbol_dac)
    bufI.append(i)
    bufQ.append(q)

# resynchronisation pour compenser le retard de chargement du second buffer
decalage = 1  # 1 échantillon de retard avec fs = 1000 ou 10000 et 2000 samples
# decalage = 8  # 8 échantillons de retard avec fs = 100000 et 2000 samples
# rotation circulaire du buffer
bufQ = bufQ[decalage:]+bufQ[:decalage]

dacI = pyb.DAC(1)  # broche Arduino A2
dacI.init(8, buffering=True)

dacQ = pyb.DAC(2)  # broche Arduino D13
dacQ.init(8, buffering=True)

dacI.write_timed(bufI, fs, mode=pyb.DAC.CIRCULAR)
""" IMPORTANT !!!
Le chargement du buffer du DAC(2) se fait avec un certain retard.
Ce retard est un multiple entier de la période d'échantillonnage (1/fs).
Expérimentalement, on constate :
1 échantillon de retard avec fs = 1000 ou 10000 Hz et 2000 samples
8 échantillons de retard avec fs = 100000 et 2000 samples
=> resynchronisation obligatoire pour les constellations 8-PSK, 16-PSK, 32-QAM
(pour cela : rotation circulaire du buffer du DAC(2))
"""
dacQ.write_timed(bufQ, fs, mode=pyb.DAC.CIRCULAR)

input("Enter to quit")

dacI.deinit()
dacQ.deinit()
