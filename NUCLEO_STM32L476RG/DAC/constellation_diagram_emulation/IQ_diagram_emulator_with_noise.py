# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

# (C) Fabrice Sincère

import micropython
import machine
import pyb
import math
import random
import sys
import uos  # urandom

micropython.alloc_emergency_exception_buf(100)
PI = math.pi

__version__ = (0, 1, 0)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

print(""" Emulation d'un diagramme de constellation avec bruit gaussien

Matériel : 1 carte NUCLEO-L476RG et un oscilloscope

sortie A2  --> I (In-phase)
sortie D13 --> Q (Quadrature)

A2  à brancher sur la voie 1 de l'oscilloscope
D13 à brancher sur la voie 2 de l'oscilloscope

Oscilloscope en mode X-Y
et utilisation de la persistance.

B-PSK   --> 0
Q-PSK   --> 1
8-PSK   --> 2
16-PSK  --> 3
4-ASK   --> 4
8-ASK   --> 5
16-ASK  --> 6
16-QAM  --> 7
32-QAM  --> 8
64-QAM  --> 9
""")

while True:
    choix = input('Votre choix ? ')
    if choix in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]:
        choix = int(choix)
        break
    print("Choix invalide")

if choix == 0:
    # B-PSK
    # I, Q
    symbol = [[-1, 0], [1, 0]]
    Imin, Imax = -3, 3  # 0, 255
    Qmin, Qmax = -3, 3  # 0, 255

elif choix == 1:
    # Q-PSK
    # I, Q
    symbol = [[-1, -1], [-1, 1], [1, -1], [1, 1]]
    Imin, Imax = -3, 3  # 0, 255
    Qmin, Qmax = -3, 3  # 0, 255

elif choix == 2:
    # 8-PSK
    # I, Q
    symbol = [[math.cos(PI/8), math.sin(PI/8)],
              [math.cos(3*PI/8), math.sin(3*PI/8)],
              [math.cos(5*PI/8), math.sin(5*PI/8)],
              [math.cos(7*PI/8), math.sin(7*PI/8)],
              [math.cos(-PI/8), math.sin(-PI/8)],
              [math.cos(-3*PI/8), math.sin(-3*PI/8)],
              [math.cos(-5*PI/8), math.sin(-5*PI/8)],
              [math.cos(-7*PI/8), math.sin(-7*PI/8)]]
    Imin, Imax = -2, 2  # 0, 255
    Qmin, Qmax = -2, 2  # 0, 255

elif choix == 3:
    # 16-PSK
    # I, Q
    symbol = [[math.cos(PI/16), math.sin(PI/16)],
              [math.cos(3*PI/16), math.sin(3*PI/16)],
              [math.cos(5*PI/16), math.sin(5*PI/16)],
              [math.cos(7*PI/16), math.sin(7*PI/16)],
              [math.cos(9*PI/16), math.sin(9*PI/16)],
              [math.cos(11*PI/16), math.sin(11*PI/16)],
              [math.cos(13*PI/16), math.sin(13*PI/16)],
              [math.cos(15*PI/16), math.sin(15*PI/16)],
              [math.cos(-PI/16), math.sin(-PI/16)],
              [math.cos(-3*PI/16), math.sin(-3*PI/16)],
              [math.cos(-5*PI/16), math.sin(-5*PI/16)],
              [math.cos(-7*PI/16), math.sin(-7*PI/16)],
              [math.cos(-9*PI/16), math.sin(-9*PI/16)],
              [math.cos(-11*PI/16), math.sin(-11*PI/16)],
              [math.cos(-13*PI/16), math.sin(-13*PI/16)],
              [math.cos(-15*PI/16), math.sin(-15*PI/16)]]
    Imin, Imax = -2, 2  # 0, 255
    Qmin, Qmax = -2, 2  # 0, 255

elif choix == 4:
    # 4-ASK
    # I, Q
    symbol = [[-3, 0], [-1, 0], [1, 0], [3, 0]]
    Imin, Imax = -5, 5  # 0, 255
    Qmin, Qmax = -5, 5  # 0, 255

elif choix == 5:
    # 8-ASK
    # I, Q
    symbol = [[-7, 0], [-5, 0], [-3, 0], [-1, 0],
              [1, 0], [3, 0], [5, 0], [7, 0]]
    Imin, Imax = -9, 9  # 0, 255
    Qmin, Qmax = -9, 9  # 0, 255

elif choix == 6:
    # 16-ASK
    # I, Q
    symbol = [[-15, 0], [-13, 0], [-11, 0], [-9, 0],
              [-7, 0], [-5, 0], [-3, 0], [-1, 0],
              [1, 0], [3, 0], [5, 0], [7, 0],
              [9, 0], [11, 0], [13, 0], [15, 0]]
    Imin, Imax = -17, 17  # 0, 255
    Qmin, Qmax = -17, 17  # 0, 255


elif choix == 7:
    # 16-QAM
    # I, Q
    symbol = [[-3, -3], [-3, -1], [-3, 1], [-3, 3],
              [-1, -3], [-1, -1], [-1, 1], [-1, 3],
              [1, -3], [1, -1], [1, 1], [1, 3],
              [3, -3], [3, -1], [3, 1], [3, 3]]
    Imin, Imax = -5, 5  # 0, 255
    Qmin, Qmax = -5, 5  # 0, 255

elif choix == 8:
    # 32-QAM
    # I, Q
    symbol = [[-3, -3], [-3, -1], [-3, 1], [-3, 3],
              [-1, -3], [-1, -1], [-1, 1], [-1, 3],
              [1, -3], [1, -1], [1, 1], [1, 3],
              [3, -3], [3, -1], [3, 1], [3, 3],
              [-5, -3], [-5, -1], [-5, 1], [-5, 3],
              [5, -3], [5, -1], [5, 1], [5, 3],
              [-3, 5], [-1, 5], [1, 5], [3, 5],
              [-3, -5], [-1, -5], [1, -5], [3, -5]]
    Imin, Imax = -7, 7  # 0, 255
    Qmin, Qmax = -7, 7  # 0, 255

elif choix == 9:
    # 64-QAM
    # I, Q
    symbol = [[-7, -7], [-5, -7], [-3, -7], [-1, -7],
              [1, -7], [3, -7], [5, -7], [7, -7],
              [-7, -5], [-5, -5], [-3, -5], [-1, -5],
              [1, -5], [3, -5], [5, -5], [7, -5],
              [-7, -3], [-5, -3], [-3, -3], [-1, -3],
              [1, -3], [3, -3], [5, -3], [7, -3],
              [-7, -1], [-5, -1], [-3, -1], [-1, -1],
              [1, -1], [3, -1], [5, -1], [7, -1],
              [-7, 1], [-5, 1], [-3, 1], [-1, 1],
              [1, 1], [3, 1], [5, 1], [7, 1],
              [-7, 3], [-5, 3], [-3, 3], [-1, 3],
              [1, 3], [3, 3], [5, 3], [7, 3],
              [-7, 5], [-5, 5], [-3, 5], [-1, 5],
              [1, 5], [3, 5], [5, 5], [7, 5],
              [-7, 7], [-5, 7], [-3, 7], [-1, 7],
              [1, 7], [3, 7], [5, 7], [7, 7]]
    Imin, Imax = -9, 9  # 0, 255
    Qmin, Qmax = -9, 9  # 0, 255

# mise à l'échelle
# DAC 8 bits 0 -> 0 V    255 -> 3.3 V

symbol_dac = []

for val in symbol:
    i, q = val
    i_dac = int((i-Imin)*255/(Imax-Imin))
    q_dac = int((q-Qmin)*255/(Qmax-Qmin))

    symbol_dac.append([i_dac, q_dac])

# calcul de la tension efficace du signal (composante alternative)
Vsignal_rms = 0  # en volts
for val in symbol_dac:
    i, q = val
    Vsignal_rms += ((i-127.5)**2+(q-127.5)**2)  # 255/2
Vsignal_rms = math.sqrt(Vsignal_rms/len(symbol))*3.3/255  # 255 -> 3.3 V

print("Signal : {} Vrms".format(Vsignal_rms))

# Signal Noise Ratio (dB)
# pas trop grand (à cause du bruit de quantification du DAC 8 bits
# pas trop petit (limitation à la plage 0-3.3V)
while True:
    try:
        snr = float(input("Signal Noise Ratio (dB) ? "))
        Vnoise_rms = Vsignal_rms/10**(snr/20)
        break
    except ValueError:
        print("Invalid value")

print("Noise : {} Vrms".format(Vnoise_rms))

# create a buffer
# nombre d'échantillons
# buffer size
samples = 2500  # int

# sampling frequency Hz
# symbol rate (baud)
fs = 1250  # int
print("Symbol rate : {} bauds".format(fs))

# durée d'un cycle (en ms)
cycle_duration = samples*1000/fs  # int
if cycle_duration != int(samples*1000/fs):
    print("Durée d'un cycle : {} ms".format(cycle_duration))
    print("ERREUR : La durée d'un cycle doit être un nombre entier de ms")
    print("Veuillez modifier la valeur des variables samples et fs")
    sys.exit(1)

cycle_duration = int(cycle_duration)
print("Durée d'un cycle : {} ms".format(cycle_duration))

bufI = bytearray(samples)
bufQ = bytearray(samples)

# resynchronisation pour compenser le retard de chargement du second buffer
# rotation circulaire du buffer
decalage = 1  # 1 échantillon de retard avec fs = 1000 ou 10000 et 2000 samples
# decalage = 8  # 8 échantillons de retard avec fs = 100000 et 2000 samples

dacI = pyb.DAC(1)  # broche Arduino A2
dacI.init(8, buffering=True)

dacQ = pyb.DAC(2)  # broche Arduino D13
dacQ.init(8, buffering=True)


def cb(e):
    # timer
    global time_elapsed
    for j in range(samples):
        i, q = random.choice(symbol_dac)

        # ajout du bruit gaussien
        # loi normale approximée par la somme de 16 octets aléatoires
        # d'une loi uniforme (Théorème central limite)
        # sum(uos.urandom(16))-2040)  # centré sur 0
        # loi uniforme 0-255 : écart-type 255/12**0.5 = 73.6
        # somme de 16 tirages : écart-type 73.6*16**0.5 = 294.44
        # valeur efficace (= écart-type) = 294.44*3.3/255 = 3.8105 Vrms
        # bruit sur les deux axes : 3.8105*2**0.5 = 5.3888 Vrms
        i += int((sum(uos.urandom(16))-2040)*Vnoise_rms/5.3888+0.5)
        q += int((sum(uos.urandom(16))-2040)*Vnoise_rms/5.3888+0.5)

        # DAC roll over
        i = max(0, min(i, 255))  # 0-255 range
        q = max(0, min(q, 255))  # 0-255 range
        bufI[j] = i
        # resynchronisation
        # rotation circulaire du buffer
        bufQ[(j-decalage) % samples] = q

    dacI.write_timed(bufI, fs, mode=pyb.DAC.CIRCULAR)
    """ IMPORTANT !!!
    Le chargement du buffer du DAC(2) se fait avec un certain retard.
    Ce retard est un multiple entier de la période d'échantillonnage (1/fs).
    Expérimentalement, on constate :
    1 échantillon de retard avec fs = 1000 ou 10000 Hz et 2000 samples
    8 échantillons de retard avec fs = 100000 et 2000 samples
    => resynchronisation obligatoire pour les constellations 8-PSK, 16-PSK
    et 32-QAM
    (pour cela : rotation circulaire du buffer du DAC(2))
    """
    dacQ.write_timed(bufQ, fs, mode=pyb.DAC.CIRCULAR)
    time_elapsed += cycle_duration
    print("Temps écoulé : {} s".format(time_elapsed/1000), end='\r')


# timer période en ms (int)
time_elapsed = 0  # ms
timer = machine.Timer()
timer.init(mode=machine.Timer.PERIODIC, period=cycle_duration, callback=cb)

input("Enter to quit\n")
print()

# disable cpu timer
timer.deinit()

dacI.deinit()
dacQ.deinit()
