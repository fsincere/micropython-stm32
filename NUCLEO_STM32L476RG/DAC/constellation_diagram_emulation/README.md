## Carte NUCLEO STM32L476RG

### Emulation du diagramme de constellation d'une modulation numérique

#### Script MicroPython : IQ_diagram_emulator.py

```
>>>
Matériel : 1 carte NUCLEO-L476RG et un oscilloscope

sortie A2  --> I (In-phase)
sortie D13 --> Q (Quadrature)

A2  à brancher sur la voie 1 de l'oscilloscope
D13 à brancher sur la voie 2 de l'oscilloscope

Oscilloscope en mode X-Y
et utilisation de la persistance.

B-PSK   --> 0
Q-PSK   --> 1
8-PSK   --> 2
16-PSK  --> 3
4-ASK   --> 4
8-ASK   --> 5
16-ASK  --> 6
16-QAM  --> 7
32-QAM  --> 8
64-QAM  --> 9

Votre choix ? 2
Enter to quit
>>>
```

- 8-PSK

![8PSK](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/DAC/constellation_diagram_emulation/images/8PSK.png)

- 16-QAM

![16QAM](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/DAC/constellation_diagram_emulation/images/16QAM.png)

#### Script MicroPython : IQ_diagram_emulator_with_noise.py

- Q-PSK avec bruit gaussien

![QPSK](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/DAC/constellation_diagram_emulation/images/QPSK.png)

- 32-QAM avec bruit gaussien

![32QAM](https://framagit.org/fsincere/micropython-stm32/-/raw/main/NUCLEO_STM32L476RG/DAC/constellation_diagram_emulation/images/32QAM.png)


