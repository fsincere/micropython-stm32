from pyb import Pin
import time

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

led = Pin("D13")

# CTRL+C pour quitter

while True:
    # pause de 500 ms
    time.sleep_ms(500)
    led(0)
    print(led())
    time.sleep_ms(500)
    led(1)
    print(led())
