# (C) Fabrice SINCERE
# MicroPython v1.17 on 2021-10-30; NUCLEO-L476RG with STM32L476RG

"""
STM32L476   MicroPython v1.17

attention : il faut utiser 'machine' et pas 'pyb'
(machine.I2C et pyb.I2C ne sont pas complètement compatible)

# bus i2c(1)
# D14 (arduino) pin -> SDA  + r pullup 2.2k
# D15 (arduino) pin -> SCL  + r pullup 2.2k

# bus i2c(2)
# B11 (morpho) pin -> SDA  + r pullup 2.2k
# D6 (arduino) pin -> SCL  + r pullup 2.2k

# bus i2c(3)
# A4 (arduino) pin -> SDA  + r pullup 2.2k
# A5 (arduino) pin -> SCL  + r pullup 2.2k
"""

from machine import I2C

__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

i2c = I2C(1)
# i2c = I2C(2)
# i2c = I2C(3)

# i2c bus scan
print("Scan du bus I2c :\n")
[print(hex(i)) for i in i2c.scan()]
print("Fin du scan")
