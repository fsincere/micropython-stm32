## Carte NUCLEO STM32L476RG

Cette carte possède 3 bus I2C : 

- bus i2c(1)

D14 (arduino) pin -> SDA  + pull-up resistance 2.2 kΩ (à ajouter)

D15 (arduino) pin -> SCL  + pull-up resistance 2.2 kΩ (à ajouter)

- bus i2c(2)

B11 (morpho) pin -> SDA  + pull-up resistance 2.2 kΩ (à ajouter)

D6 (arduino) pin -> SCL  + pull-up resistance 2.2 kΩ (à ajouter)

- bus i2c(3)

A4 (arduino) pin -> SDA  + pull-up resistance 2.2 kΩ (à ajouter)

A5 (arduino) pin -> SCL  + pull-up resistance 2.2 kΩ (à ajouter)
