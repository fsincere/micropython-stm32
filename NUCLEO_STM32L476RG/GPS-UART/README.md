## Carte NUCLEO STM32L476RG : module GPS avec liaison série UART

3 fils à brancher entre la carte NUCLEO et le module GPS neo-6M (GY-GPS6MV2)

- GND
- Alimentation Vcc 3.3 V du module GPS
- Broche TX du module GPS à relier à la broche A1 (RX de l'UART4)
  de la carte NUCLEO-L476RG


![module GPS neo-6M](images/moduleGPSneo-6M(GY-GPS6MV2).jpg)


