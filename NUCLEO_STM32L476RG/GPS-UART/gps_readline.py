# MicroPython v1.17 on 2021-10-30; NUCLEO-L476RG with STM32L476RG
# (C) Fabrice Sincère
# test OK

import time
from pyb import UART

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

print("""
3 fils à brancher :
- GND
- Alimentez le module GPS neo-6M (GY-GPS6MV2) en 3.3 V
- Reliez la broche TX du module GPS à la broche A1 (RX de l'UART4)
  de la carte NUCLEO-L476RG
""")

input("ENTER pour continuer")

# IMPORTANT : timeout = 10 (au lieu de 0)
# autrement, on recoit la trame par tronçon de 256 octets max
u = UART(4, baudrate=9600, bits=8, parity=None, stop=1,
         flow=0, timeout=10, timeout_char=3, rxbuf=64)

print("CTRL+C pour quitter\n")

try:
    while True:
        reception = u.readline()  # type bytes
        if reception is not None:
            # print(reception)  # type bytes
            print("Réception trame {} octets :".format(len(reception)))
            try:
                print(reception.decode())  # str
                pass
            except UnicodeError:
                pass
        else:
            time.sleep_ms(100)
except KeyboardInterrupt:
    print("\nBye !")

"""
Réception trame 68 octets :
$GPRMC,082549.00,A,4415.78668,N,00435.76047,E,1.106,,041121,,,A*70

Réception trame 35 octets :
$GPVTG,,T,,M,1.106,N,2.049,K,A*2A

Réception trame 74 octets :
$GPGGA,082549.00,4415.78668,N,00435.76047,E,1,08,1.26,94.1,M,47.7,M,,*68

Réception trame 58 octets :
$GPGSA,A,3,01,19,31,21,06,04,17,22,,,,,2.52,1.26,2.18*05

Réception trame 70 octets :
$GPGSV,3,1,10,01,52,125,22,03,72,027,26,04,59,182,28,06,16,308,23*7F

Réception trame 70 octets :
$GPGSV,3,2,10,09,25,207,19,17,48,264,27,19,39,296,20,21,32,132,26*7E

Réception trame 44 octets :
$GPGSV,3,3,10,22,54,067,20,31,19,059,10*7D

Réception trame 52 octets :
$GPGLL,4415.78668,N,00435.76047,E,082549.00,A,A*68
"""
