# MicroPython v1.17 on 2021-10-30; NUCLEO-L476RG with STM32L476RG
# (C) Fabrice Sincère

import time
from pyb import UART

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

print("""
3 fils à brancher :
- GND
- Alimentez le module GPS neo-6M (GY-GPS6MV2) en 3.3 V
- Reliez la broche TX du module GPS à la broche A1 (RX de l'UART4)
  de la carte NUCLEO-L476RG
""")


def decodage_trame(data):
    # data -> str
    # ex de début de trame :
    # $GPRMC,080902.00,A,4415.78551,N,00435.76895,E,0.714,,031121,,,A*722,K,A*23
    # $GPGGA,080902.00,4415.78551,N,00435.76895,  etc

    data = data.replace('\r\n', ',')  # on supprime les sauts de ligne

    # ['$GPRMC', '080902.00', 'A', '4415.78551', 'N', '00435.76895', 'E',
    # '0.714', '','031121', '', '', 'A*722', 'K', 'A*23', '$GPGGA', '080902.00',
    # '4415.78551', 'N', '00435.76895' etc

    datas = data.split(",")
    if datas[0] != '$GPRMC':
        return '?', '?', '?', '?', '?', '?', '?'
    heure = datas[1]  # str '080902.00'  08:09:02.00
    latitude = datas[3]  # str '4415.78551'
    latitude_deg = latitude[:2]  # '44' degrés
    latitude_min = latitude[2:]  # '15.78551' minutes d'angle

    longitude = datas[5]  # str '00435.76895'
    longitude_deg = longitude[:3]  # '004' degrés
    longitude_min = longitude[3:]  # '35.76895' minutes d'angle

    date = datas[9]  # str 031121 date au format ddMMYY
    # on recherche le début de la trame GGA
    try:
        indice = datas.index('$GPGGA')
        altitude = datas[indice+9]
    except ValueError:
        altitude = '?'

    return date, heure, latitude_deg, latitude_min, longitude_deg,\
        longitude_min, altitude


input("ENTER pour continuer")

print("date   heure     longitude    latitude      altitude")

# IMPORTANT : timeout = 10 (au lieu de 0)
# autrement, on recoit la trame par tronçon de 256 octets max
u = UART(4, baudrate=9600, bits=8, parity=None, stop=1, flow=0, timeout=10,
         timeout_char=3, rxbuf=64)

print("CTRL+C pour quitter\n")

try:
    while True:
        reception = u.read()  # type bytes
        if reception is not None:
            # print(reception)  # type bytes
            # print("Réception trame {} octets :".format(len(reception)))
            try:
                data = reception.decode()  # str
                # print(data)  # str
                date,  heure, latitude_deg, latitude_min, longitude_deg, longitude_min, altitude = decodage_trame(data)
                print("{} {} {}°{}' {}°{}' {} m".format(date, heure, latitude_deg,
                                                        latitude_min, longitude_deg,
                                                        longitude_min, altitude))

            except UnicodeError:
                pass
        else:
            time.sleep_ms(100)
except KeyboardInterrupt:
    print("\nBye !")

"""
ENTER pour continuer
date   heure     longitude    latitude      altitude
110223 092207.00 44°15.78552' 004°35.76575' 88.8 m
110223 092208.00 44°15.78530' 004°35.76584' 89.2 m
110223 092209.00 44°15.78552' 004°35.76592' 89.4 m
"""
