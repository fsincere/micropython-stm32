# MicroPython v1.17 on 2021-10-30; NUCLEO-L476RG with STM32L476RG
# (C) Fabrice Sincère

import time
from pyb import UART

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

print("""
3 fils à brancher :
- GND
- Alimentez le module GPS neo-6M (GY-GPS6MV2) en 3.3 V
- Reliez la broche TX du module GPS à la broche A1 (RX de l'UART4)
  de la carte NUCLEO-L476RG
""")

input("ENTER pour continuer")

# IMPORTANT : timeout = 10 (au lieu de 0)
# autrement, on recoit la trame par tronçon de 256 octets max
u = UART(4, baudrate=9600, bits=8, parity=None, stop=1,
         flow=0, timeout=10, timeout_char=3, rxbuf=64)

print("CTRL+C pour quitter\n")

try:
    while True:
        reception = u.read()  # type bytes
        if reception is not None:
            # print(reception)  # type bytes
            print("Réception trame {} octets :".format(len(reception)))
            try:
                print(reception.decode())  # str
                pass
            except UnicodeError:
                pass
        else:
            time.sleep_ms(100)
except KeyboardInterrupt:
    print("\nBye !")
"""
// toutes les 1 seconde on reçoit automatiquement une trame ascii
// à la mise sous tension de gps, on recoit aucune info :
Réception trame 140 octets :
$GPRMC,,V,,,,,,,,,,N*53
$GPVTG,,,,,,,,,N*30
$GPGGA,,,,,,0,00,99,,,,,,,99.99,99.99,99.99*30
$GPGSV,1,1,01,03,,,30*78
$GPGLL,,,,,,V,N*64

// un peu plus tard, on a la date et l'heure :
Réception trame 203 octets :
$GPRMC,072319.00,V,,,,,,,031121,,,N*73
$GPVTG,,,,,,,,,N*30
$GPGGA,072319.00,,,,,0,00,99.99,,,,,,*68
$GPGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*30
$GPGSV,1,1,01,03,,,25*7C
$GPGLL,,,,,072319.00,V,N*44

// et enfin réception OK :
// (la led bleue du module clignote)

// la trame totale fait environ 500 octets
// soit environ une durée de 500 ms (9600 bauds)

Réception trame 489 octets :
$GPRMC,075841.00,A,4415.78758,N,00435.76328,E,0.154,,031121,,,A*74
$GPVTG,,T,,M,0.154,N,0.286,K,A*2F
$GPGGA,075841.00,4415.78758,N,00435.76328,E,1,06,1.68,94.1,M,47.7,M,,*69
$GPGSA,A,3,01,21,04,22,17,19,,,,,,,4.19,1.68,3.84*06
$GPGSV,3,1,12,01,66,108,23,03,77,332,30,04,43,184,27,06,05,303,18*72
$GPGSV,3,2,12,09,12,204,15,14,01,251,,17,48,285,26,19,31,310,19*70
$GPGSV,3,3,12,21,44,123,15,22,65,052,25,31,17,073,16,32,02,037,*7E
$GPGLL,4415.78758,N,00435.76328,E,075841.00,A,A*6D

// au format bytes :
b'$GPRMC,080225.00,A,4415.79283,N,00435.76777,E,0.423,,031121,,,A*7F\r\n$GPVTG,,T,,M,0.423,N,0.783,K,A*2A\r\n$GPGGA,080225.00,4415.79283,N,00435.76777,E,1,06,1.63,104.7,M,47.7,M,,*52\r\n$GPGSA,A,3,01,21,04,22,17,19,,,,,,,4.64,1.63,4.35*0A\r\n$GPGSV,3,1,12,01,64,111,20,03,77,340,27,04,45,184,23,06,06,304,18*7E\r\n$GPGSV,3,2,12,09,13,205,,14,00,250,,17,48,283,29,19,32,309,25*79\r\n$GPGSV,3,3,12,21,43,124,20,22,64,054,19,31,17,071,,32,01,037,*76\r\n$GPGLL,4415.79283,N,00435.76777,E,080225.00,A,A*63\r\n'

Réception trame 486 octets :
$GPRMC,080225.00,A,4415.79283,N,00435.76777,E,0.423,,031121,,,A*7F
$GPVTG,,T,,M,0.423,N,0.783,K,A*2A
$GPGGA,080225.00,4415.79283,N,00435.76777,E,1,06,1.63,104.7,M,47.7,M,,*52
$GPGSA,A,3,01,21,04,22,17,19,,,,,,,4.64,1.63,4.35*0A
$GPGSV,3,1,12,01,64,111,20,03,77,340,27,04,45,184,23,06,06,304,18*7E
$GPGSV,3,2,12,09,13,205,,14,00,250,,17,48,283,29,19,32,309,25*79
$GPGSV,3,3,12,21,43,124,20,22,64,054,19,31,17,071,,32,01,037,*76
$GPGLL,4415.79283,N,00435.76777,E,080225.00,A,A*63
"""
