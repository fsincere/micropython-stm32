## Carte NUCLEO STM32L476RG : capteur BME280 (Bosch Sensortec)

Le BME280 est un capteur numérique de température, humidité et pression atmosphérique.  

#### Plage de mesures

+ Température -40 à +85 °C
+ Humidité relative de l'air (hygrométrie) : 0 à 100 %
+ Pression 300 à 1100 hPa

#### Précision des mesures

+ Température ±0.5 °C (0 … 65 °C)
+ Humidité relative ± 3 %
+ Pression ± 1,0 hPa (300 ... 1100 hPa ; 0 ... 65 °C)

![BME280-sensor](images/BME280-sensor.jpg)

Sur cette carte, le capteur est entouré de quelques résistances et condensateurs :  

![BME280-schematic](images/BME280-schematic.jpg)


Le BME280 possède une interface SPI et une interface I2C.  

Ici, il ne sera question que du bus i2c.  

Deux adresses I2C :   

- par défaut : 0x76  (SDO non connectée,  0 V)  
- 0x77  (SDO reliée à 3,3 V)  

4 fils à brancher :

- GND
- Alimentation Vcc 3,3 V
- Broche SDA du BME280 à relier à la broche D14 de la carte NUCLEO-L476RG
- Broche SCL du BME280 à relier à la broche D15 de la carte NUCLEO-L476RG

Pas besoin de résistances de pull-up sur les broches SDA et SCL car elles sont déja présentes sur la carte BME280 (R2 et R3, 10 kΩ).


## Le driver i2c MicroPython pour le capteur BME280

Le module MicroPython bme280.py gère la communication i2c avec le capteur BME280.  

Je l'ai adapté des deux projets suivants :  

[https://github.com/robert-hh/BME280](https://github.com/robert-hh/BME280)  

[https://github.com/adafruit/Adafruit_CircuitPython_BME280](https://github.com/adafruit/Adafruit_CircuitPython_BME280)  


### Utilisation

On commence pour uploader dans la mémoire flash du microcontrôleur STM32L476RG le module bme280.py  

A noter que seul le mode "normal" du BME280 est proposé par ce module (dans ce mode, les mesures sont périodiques).  

```Python
>>> from machine import I2C
>>> from bme280 import *

>>> i2c = I2C(1)

>>> bme = BME280(oversampling=BME280_OSAMPLE_2,
                 iir=BME280_IIR_OFF,
                 standby=BME280_STANDBY_20,
                 i2c=i2c, address=0x76)

>>> print(bme.values())
('19.70 C', '1011.764 hPa', '61.59 %')
```

### Bruit de mesures

Voici le résultat de 500 mesures de pression (avec oversampling x2 sans filtre iir) :  

![pressure_500ech_x2_no_iir_chronogramme](images/pressure_500ech_x2_no_iir_chrono.png)

![pressure_500ech_x2_no_iir](images/pressure_500ech_x2_no_iir.png)

Moyenne	: 25279599,3/256 = 98748,43 Pa  

RMS noise (écart-type) : 2,58 Pa (2,6 Pa d'après le datasheet :)  

On constate que le bruit est gaussien (loi normale).  

### Oversampling

L'oversampling peut prendre les valeurs x1, x2, x4, x8 ou x16 (BME280_OSAMPLE_1, BME280_OSAMPLE_2, BME280_OSAMPLE_4, BME280_OSAMPLE_8 ou BME280_OSAMPLE_16).  

x1 pour une mesure de base.  
xN revient à faire une moyenne de N mesures, d'où une meilleure résolution et une réduction du bruit.  
Par contre, la durée de conversion est N fois plus grande.  

On peut individualiser l'oversampling des mesures de température, pression et humidité (dans cet ordre) :  
```Python
oversampling=(BME280_OSAMPLE_8, BME280_OSAMPLE_16, BME280_OSAMPLE_1),
```
On peut aussi désactiver certaines mesures.  
Par exemple, pour ignorer la mesure d'humidité :  
```Python
oversampling=(BME280_OSAMPLE_8, BME280_OSAMPLE_16, BME280_OSAMPLE_OFF),
```

### IIR low-pass filter

On peut activer un filtre numérique récursif passe-bas interne.  

Attention, **cela ne concerne que la mesure de température et de pression**.  

L'équation de récurrence (Cf. datasheet du BME280) est :  

```
y(n) = (y(n-1)*(a-1) + x(n))/a
```
x(n) désigne la dernière valeur du convertisseur ADC, et y(n) la sortie du filtre numérique.  

Le coefficient *a* peut prendre les valeurs 2, 4, 8 ou 16 (iir=BME280_IIR_2, BME280_IIR_4, BME280_IIR_8 ou BME280_IIR_16).  

Quand le coefficient *a* augmente, la fréquence de coupure du filtre diminue, le lissage des mesures est plus efficace et cela se traduit également par une diminution du bruit.  
Par contre, le temps de réponse augmente.  

Pour désactiver le filtre interne :  
```Python
iir = BME280_IIR_OFF,
```

### Résolution du convertisseur ADC

Pour la mesure d'humidité, la résolution est dans tous les cas 16 bits.  

Pour les mesures de température et de pression, la résolution est de 20 bits quand le filtre iir est activé.  

Autrement, elle va de 16 bits (oversampling x1) à 20 bits (oversampling x16).



### Correction des mesures

Pour une meilleure précision, les données brutes (raw datas) fournies par le convertisseur ADC doivent être corrigées (compensation de l'effet de la température sur la mesure de la pression et de l'humidité).  
La procédure utilisée est celle proposée par le fabricant et elle donne un résultat sur 32 bits.  
Elle tient compte des données de calibration interne propre à chaque BME280 (Cf. datasheet).  

```Python
>>> temperature, pressure, humidity = bme.read_compensated_data()

>>> print(temperature, pressure, humidity)
1970 25902306 63296
```
La température s'obtient ensuite en divisant par 100 : 1970/100 = 19,70 °C,  
la pression en divisant par 256 : 25902306/256 = 101180,8828125 Pa,  
et l'humidité en divisant par 1024 : 63296/1024 = 61,8125 %RH

```Python
>>> print(temperature/100, pressure/256, humidity/1024)
19.7 101180.9 61.8125
```
Ci-dessus, les calculs sont donnés avec la précision machine, soit environ 7 chiffres significatifs (float).  
Pour un calcul exact :  

```Python
>>> temperature, pressure, humidity = bme.read_compensated_data(printfloat=True)
19.70 101180.88281250 61.8125000000
```

La même chose avec un format différent :  
```Python
>>> print(bme.values())
('19.70 C', '1011.809 hPa', '61.81 %')
```

Pour information, on peut lire les données brutes du convertisseur ADC de la manière suivante :  


```Python
>>> from array import array
>>> result = array("i", [0, 0, 0])
>>> bme.read_raw_data(result)
>>> raw_temp, raw_press, raw_hum = result
>>> print(raw_temp, raw_press, raw_hum)
512494 319649 34902
```

Puis, pour obtenir les valeurs compensées correspondantes :  

```Python
>>> temperature, pressure, humidity = bme.read_compensated_data(raw_data=(raw_temp, raw_press, raw_hum), printfloat=True)
20.23 100875.97656250 64.2314453125
>>> print(temperature, pressure, humidity)
2023 25824250 65773
>>> values = bme.values(compensated_data=(temperature, pressure, humidity))
>>> print(values)
('20.23 C', '1008.760 hPa', '64.23 %')
```


### Standby time

C'est le temps de pause entre deux conversions (on est en mode normal).  

8 valeurs possibles :  

```python
standby=
BME280_STANDBY_0_5   # 0.5 ms
BME280_STANDBY_10    # 10 ms
BME280_STANDBY_20    # 20 ms
BME280_STANDBY_62_5  # 62.5 ms
BME280_STANDBY_125   # 125 ms
BME280_STANDBY_250   # 250 ms
BME280_STANDBY_500   # 500 ms
BME280_STANDBY_1000  # 1000 ms
```

### Fréquence d'échantillonnage

La durée d'un cycle de mesures est l'addition de la durée d'acquisition et de la durée de standby.  

La durée d'acquisition ne dépend que du choix de l'oversampling.  

Avec un oversampling x2 (oversampling=BME280_OSAMPLE_2) et un standby de 20 ms (standby=BME280_STANDBY_20), cela donne :

```Python
>>> cycle_time = bme.cycle_time
>>> print("cycle time :", cycle_time, "ms")
cycle time : 36 ms
```
soit 28 mesures par seconde.  

La durée d'acquisition vaut donc 36-20 = 16 ms (pour l'ensemble des 3 mesures).  

Pour échantillonner au plus vite, on choisira oversampling x1 et standby de 0,5 ms. La durée d'un cycle vaut alors 9 ms (une centaine de mesures par seconde).  

Si on veut un bruit minimal, on choisira un oversampling x16 et un filtrage numérique avec coefficient 16.  

### Altimètre

La pression atmosphérique diminue avec l'altitude.  
On connaît la relation (approximative) entre ces deux grandeurs, on peut alors estimer l'altitude absolue à partir de la mesure de la pression.  

```Python
>>> bme.sealevel = 101325  # en pascal # à ajuster
>>> print(bme.values())
('20.01 C', '1010.430 hPa', '62.46 %')
>>> print(bme.altitude)  # en mètre
23.50567
```

La mesure de variation d'altitude (hauteur ou dénivellation) est par contre beaucoup plus précise : la précision est liée au bruit de mesure soit 0,2 Pa dans le meilleur des cas. Cela correspond à une incertitude de 1,7 cm (variation d'environ -12 Pa/m).  

On peut donc espérer faire des mesures à quelques centimètres près...  




Un exemple concret en élevant le capteur de 73 cm (script *bme280_demo_altimetre.py*) :  

![altimetre](images/altimetre.png)


En complément :  
```Python
>>> from bme280 import *
>>> BME280.pressure_to_altitude(pressure=99000, sealevel=101325)
195.3961
>>> BME280.altitude_to_pressure(altitude=195.39, sealevel=101325)
99000.0
```

### Lien utile

[https://github.com/boschsensortec/BME280_SensorAPI](https://github.com/boschsensortec/BME280_SensorAPI)
