# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG
# test OK

from machine import I2C
from bme280 import *
from time import sleep_ms

__version__ = (0, 0, 3)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

# bus i2c(1)
# D14 (arduino) pin -> SDA
# D15 (arduino) pin -> SCL
i2c = I2C(1)

# i2c bus scan
print("I2c bus scan :")
[print(hex(i)) for i in i2c.scan()]

# oversampling = oversampling_temp, oversampling_press, oversampling_humidity
# iir low-pass filter coefficient
# standy time (ms)

bme = BME280(oversampling=(BME280_OSAMPLE_1, BME280_OSAMPLE_16, BME280_OSAMPLE_1),
             iir=BME280_IIR_16,
             standby=BME280_STANDBY_125,
             i2c=i2c, address=0x76)

cycle_time = bme.cycle_time
print("cycle time :", cycle_time, "ms")

print("CTRL+C to quit")
sleep_ms(1000)

while True:
    # temperature, pressure, humidity = bme.read_compensated_data()
    # temperature : value 2534 <=> 25.34 °C
    # pressure : 24674867 <=>  24674867/256 = 96386.2 Pa
    # humidity: 47445 <=> 47445/1024 = 46.333 %RH
    # print(temperature, pressure, humidity)
    print(bme.values())
    sleep_ms(cycle_time)

""" result :
I2c bus scan :
0x76
cycle time : 168 ms
CTRL+C to quit
('22.00 C', '1010.305 hPa', '58.99 %')
('21.98 C', '1010.304 hPa', '59.01 %')
('21.97 C', '1010.303 hPa', '59.02 %')
"""
