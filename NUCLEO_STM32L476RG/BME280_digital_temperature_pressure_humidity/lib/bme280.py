# Updated 2024   Fabrice Sincère
# MicroPython v1.19.1 on 2023-02-11; NUCLEO-L476RG with STM32L476RG
# MicroPython v1.17 on 2021-11-10; NUCLEO-L476RG with STM32L476RG

# source : Robert Hammelrath
# https://github.com/robert-hh/BME280
# source : Adafruit
# https://github.com/adafruit/Adafruit_CircuitPython_BME280

# Updated 2018 and 2020
# This module is based on the below cited resources, which are all
# based on the documentation as provided in the Bosch Data Sheet and
# the sample implementation provided therein.
#
# Final Document: BST-BME280-DS002-15
#
# Authors: Paul Cunnane 2016, Peter Dahlebrg 2016
#
# This module borrows from the Adafruit BME280 Python library. Original
# Copyright notices are reproduced below.
#
# Those libraries were written for the Raspberry Pi. This modification is
# intended for the MicroPython and esp8266 boards.
#
# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola
#
# Based on the BMP280 driver with BME280 changes provided by
# David J Taylor, Edinburgh (www.satsignal.eu)
#
# Based on Adafruit_I2C.py created by Kevin Townsend.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

import time
from ustruct import unpack
from array import array

__version__ = (0, 1, 9)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

# BME280 default i2c address
BME280_I2CADDR = const(0x76)

# Registers address
BME280_REGISTER_CONTROL_HUM = const(0xF2)
BME280_REGISTER_STATUS = const(0xF3)
BME280_REGISTER_CONTROL = const(0xF4)
BME280_REGISTER_CONFIG = const(0xF5)
BME280_REGISTER_RESET = const(0xE0)
BME280_REGISTER_ID = const(0xD0)

# Oversampling
# humidity    bit 2, 1, 0 BME280_REGISTER_CONTROL_HUM
# temperature bit 7, 6, 5 BME280_REGISTER_CONTROL
# pression    bit 4, 3, 2 BME280_REGISTER_CONTROL
BME280_OSAMPLE_OFF = const(0x00)    # measurement skipped
BME280_OSAMPLE_1 = const(0x01)    # oversampling ×1
BME280_OSAMPLE_2 = const(0x02)    # oversampling ×2
BME280_OSAMPLE_4 = const(0x03)   # oversampling ×4
BME280_OSAMPLE_8 = const(0x04)    # oversampling ×8
BME280_OSAMPLE_16 = const(0x05)   # oversampling ×16

BME280_OSAMPLE = {
    BME280_OSAMPLE_OFF: 0,
    BME280_OSAMPLE_1: 1,
    BME280_OSAMPLE_2: 2,
    BME280_OSAMPLE_4: 4,
    BME280_OSAMPLE_8: 8,
    BME280_OSAMPLE_16: 16
}

# bit 1 bit 0 BME280_REGISTER_CONTROL
# no operation, all registers accessible, lowest power, selected after startup
MODE_SLEEP = const(0x00)
# perform one measurement, store results and return to sleep mode
MODE_FORCED = const(0x01)
# perpetual cycling of measurements and inactive periods
MODE_NORMAL = const(0x03)

# iir low_pass filter coefficient
# bit 4, 3, 2 BME280_REGISTER_CONFIG
# temperature and pressure only
BME280_IIR_OFF = const(0x00)  # filter off
BME280_IIR_2 = const(0x01)    # filter coefficient 2
BME280_IIR_4 = const(0x02)    # filter coefficient 4
BME280_IIR_8 = const(0x03)    # filter coefficient 8
BME280_IIR_16 = const(0x04)   # filter coefficient 16

# standby timeconstant values
# Controls inactive duration t standby in normal mode
# bit 7, 6, 5 BME280_REGISTER_CONFIG
BME280_STANDBY_0_5 = const(0x00)   # 0.5 ms
BME280_STANDBY_10 = const(0x06)    # 10 ms
BME280_STANDBY_20 = const(0x07)    # 20 ms
BME280_STANDBY_62_5 = const(0x01)  # 62.5 ms
BME280_STANDBY_125 = const(0x02)   # 125 ms
BME280_STANDBY_250 = const(0x03)   # 250 ms
BME280_STANDBY_500 = const(0x04)   # 500 ms
BME280_STANDBY_1000 = const(0x05)  # 1000 ms

BME280_STANDBY = {
    BME280_STANDBY_0_5: 0.5,
    BME280_STANDBY_10: 10,
    BME280_STANDBY_20: 20,
    BME280_STANDBY_62_5: 62.5,
    BME280_STANDBY_125: 125,
    BME280_STANDBY_250: 250,
    BME280_STANDBY_500: 500,
    BME280_STANDBY_1000: 1000
}


class BME280:

    def __init__(self,
                 # oversampling=(BME280_OSAMPLE_1, BME280_OSAMPLE_1, BME280_OSAMPLE_1)
                 oversampling=BME280_OSAMPLE_1,
                 iir=BME280_IIR_OFF,
                 standby=BME280_STANDBY_125,
                 address=BME280_I2CADDR,
                 i2c=None):

        self.address = address

        if self.address not in [0x76, 0x77]:
            raise ValueError("Invalid i2c address")

        if i2c is None:
            raise ValueError('An I2C object is required.')
        self.i2c = i2c

        # Check that oversampling is valid.
        if type(oversampling) is tuple and len(oversampling) == 3:
            self._oversampling_temp, self._oversampling_press, self._oversampling_hum = \
                oversampling
        elif type(oversampling) == int:
            self._oversampling_temp, self._oversampling_press, self._oversampling_hum = \
                oversampling, oversampling, oversampling
        else:
            raise ValueError("Wrong type for the oversampling parameter, \
must be int or a 3 element tuple")

        for oversampling in (self._oversampling_hum,
                             self._oversampling_temp, self._oversampling_press):
            if oversampling not in [BME280_OSAMPLE_OFF, BME280_OSAMPLE_1,
                                    BME280_OSAMPLE_2, BME280_OSAMPLE_4,
                                    BME280_OSAMPLE_8, BME280_OSAMPLE_16]:
                raise AttributeError()

        # verify chip ID
        self.verify_chip_ID()

        # reset  => set SLEEP MODE
        self.i2c.writeto_mem(self.address, BME280_REGISTER_RESET,
                             bytearray([0xB6]))
        time.sleep_ms(10)

        # set config register
        # iir filter coefficient
        self._iir_filter = iir
        # standby time
        self._t_standby = standby   # ms
        config = 0x00
        config += self._t_standby << 5
        config += self._iir_filter << 2
        self.i2c.writeto_mem(self.address, BME280_REGISTER_CONFIG,
                             bytearray([config]))

        self.__sealevel = 101325  # Pascal

        # load calibration data
        dig_88_a1 = self.i2c.readfrom_mem(self.address, 0x88, 26)
        dig_e1_e7 = self.i2c.readfrom_mem(self.address, 0xE1, 7)
        self.dig_T1, self.dig_T2, self.dig_T3, self.dig_P1, \
            self.dig_P2, self.dig_P3, self.dig_P4, self.dig_P5, \
            self.dig_P6, self.dig_P7, self.dig_P8, self.dig_P9, \
            _, self.dig_H1 = unpack("<HhhHhhhhhhhhBB", dig_88_a1)

        self.dig_H2, self.dig_H3, self.dig_H4,\
            self.dig_H5, self.dig_H6 = unpack("<hBbhb", dig_e1_e7)
        # unfold H4, H5, keeping care of a potential sign
        self.dig_H4 = (self.dig_H4 * 16) + (self.dig_H5 & 0xF)
        self.dig_H5 //= 16

        self.t_fine = 0

        # temporary data holders which stay allocated
        self._l1_barray = bytearray(1)
        self._l8_barray = bytearray(8)
        self._l3_resultarray = array("i", [0, 0, 0])

        # set hum register
        self._l1_barray[0] = self._oversampling_hum
        self.i2c.writeto_mem(self.address, BME280_REGISTER_CONTROL_HUM,
                             self._l1_barray)

        # set control register ; set MODE NORMAL
        self._l1_barray[0] = \
            self._oversampling_temp << 5 | self._oversampling_press << 2 | MODE_NORMAL
        self.i2c.writeto_mem(self.address, BME280_REGISTER_CONTROL,
                             self._l1_barray)

    def read_raw_data(self, result):
        """ Reads the raw (uncompensated) data from the sensor.

            Args:
                result: array of length 3 or alike where the result will be
                stored, in temperature, pressure, humidity order
            Returns:
                None"""

        # Wait for conversion to complete
        # while self.get_flag_conversion():
        #    time.sleep_ms(10)  # still busy

        # burst readout from 0xF7 to 0xFE, recommended by datasheet
        self.i2c.readfrom_mem_into(self.address, 0xF7, self._l8_barray)
        readout = self._l8_barray
        # pressure(0xF7): ((msb << 16) | (lsb << 8) | xlsb) >> 4
        raw_press = ((readout[0] << 16) | (readout[1] << 8) | readout[2]) >> 4
        # temperature(0xFA): ((msb << 16) | (lsb << 8) | xlsb) >> 4
        raw_temp = ((readout[3] << 16) | (readout[4] << 8) | readout[5]) >> 4
        # humidity(0xFD): (msb << 8) | lsb
        raw_hum = (readout[6] << 8) | readout[7]

        result[0] = raw_temp
        result[1] = raw_press
        result[2] = raw_hum

    def read_compensated_data(self, result=None, raw_data=None, printfloat=False):
        """ Reads the data from the sensor and returns the compensated data.

            Args:
                result: array of length 3 or alike where the result will be
                stored, in temperature, pressure, humidity order. You may use
                this to read out the sensor without allocating heap memory

            Returns:
                array with temperature, pressure, humidity. Will be the one
                from the result parameter if not None
        """
        if raw_data:
            raw_temp, raw_press, raw_hum = raw_data
        else:
            # raw_data is None
            self.read_raw_data(self._l3_resultarray)
            raw_temp, raw_press, raw_hum = self._l3_resultarray

        # temperature
        var1 = (((raw_temp // 8) - (self.dig_T1 * 2)) * self.dig_T2) // 2048
        var2 = (raw_temp // 16) - self.dig_T1
        var2 = (((var2 * var2) // 4096) * self.dig_T3) // 16384
        self.t_fine = var1 + var2
        temp = (self.t_fine * 5 + 128) // 256

        # pressure
        var1 = self.t_fine - 128000
        var2 = var1 * var1 * self.dig_P6
        var2 = var2 + ((var1 * self.dig_P5) << 17)
        var2 = var2 + (self.dig_P4 << 35)
        var1 = (((var1 * var1 * self.dig_P3) >> 8) +
                ((var1 * self.dig_P2) << 12))
        var1 = (((1 << 47) + var1) * self.dig_P1) >> 33
        if var1 == 0:
            pressure = 0
        else:
            p = ((((1048576 - raw_press) << 31) - var2) * 3125) // var1
            var1 = (self.dig_P9 * (p >> 13) * (p >> 13)) >> 25
            var2 = (self.dig_P8 * p) >> 19
            pressure = ((p + var1 + var2) >> 8) + (self.dig_P7 << 4)

        # humidity
        h = self.t_fine - 76800
        h = (((((raw_hum << 14) - (self.dig_H4 << 20) -
                (self.dig_H5 * h)) + 16384) >> 15) *
             (((((((h * self.dig_H6) >> 10) *
                (((h * self.dig_H3) >> 11) + 32768)) >> 10) + 2097152) *
              self.dig_H2 + 8192) >> 14))
        h = h - (((((h >> 15) * (h >> 15)) >> 7) * self.dig_H1) >> 4)
        h = 0 if h < 0 else h
        h = 419430400 if h > 419430400 else h
        humidity = h >> 12
        if humidity < 0:
            humidity = 0
        if humidity > 100 * 1024:
            humidity = 100 * 1024

        if result:
            result[0] = temp
            result[1] = pressure
            result[2] = humidity

            if printfloat:
                res = "{}.{:02d}".format(result[0] // 100, 100*(result[0] % 100) // 100)
                res += " {}.{:08d}".format(result[1] // 256, 100000000*(result[1] % 256) // 256)
                res += " {}.{:010d}".format(result[2] // 1024, 10000000000*(result[2] % 1024) // 1024)
                print(res)

            return result

        # result is None
        if printfloat:
            res = "{}.{:02d}".format(temp // 100, 100*(temp % 100) // 100)
            res += " {}.{:08d}".format(pressure // 256, 100000000*(pressure % 256) // 256)
            res += " {}.{:010d}".format(humidity // 1024, 10000000000*(humidity % 1024) // 1024)
            print(res)

        return array("i", (temp, pressure, humidity))

    @property
    def cycle_time(self):
        """ return cycle_time (ms) in normal mode
(standby time + maximum time required to complete a measurement)"""

        meas_time_ms = 1.25
        if self._oversampling_temp != BME280_OSAMPLE_OFF:
            meas_time_ms += 2.3 * BME280_OSAMPLE[self._oversampling_temp]
        if self._oversampling_press != BME280_OSAMPLE_OFF:
            meas_time_ms += 2.3*BME280_OSAMPLE[self._oversampling_press]+0.575
        if self._oversampling_hum != BME280_OSAMPLE_OFF:
            meas_time_ms += 2.3 * BME280_OSAMPLE[self._oversampling_hum] + 0.575
        return int(meas_time_ms + BME280_STANDBY[self._t_standby])

    def verify_chip_ID(self):
        "The id register contains the chip identification number which is 0x60"

        ID_register = self.i2c.readfrom_mem(self.address, BME280_REGISTER_ID, 1)
        if ID_register[0] == 0x60:
            return
        elif ID_register[0] in [0x56, 0x57, 0x58]:
            raise ValueError("It's not a bme280 sensor, but a bmp280 sensor")
        else:
            raise ValueError("Unknown device")

    def get_config_register(self):
        """Read and return the configuration register"""
        config_register = \
            self.i2c.readfrom_mem(self.address, BME280_REGISTER_CONFIG, 1)
        return config_register[0]

    def get_flag_conversion(self):
        """Automatically set to 1 whenever a conversion is running
and back to 0 when the results have been transferred
to the data registers"""
        # read bit 3 Register 0xF3 status
        return self.i2c.readfrom_mem(self.address, BME280_REGISTER_STATUS, 1)[0] & 0x08

    @property
    def temperature_compensation_values(self):
        """
return dig_T1, dig_T2, dig_T3
"""
        return self.dig_T1, self.dig_T2, self.dig_T3

    @property
    def pressure_compensation_values(self):
        """
return dig_P1, dig_P2, dig_P3, dig_P4, dig_P5, dig_P6, dig_P7, dig_P8, dig_P9
"""
        return self.dig_P1, \
            self.dig_P2, self.dig_P3, self.dig_P4, self.dig_P5, \
            self.dig_P6, self.dig_P7, self.dig_P8, self.dig_P9

    @property
    def humidity_compensation_values(self):
        """
return dig_H1, dig_H2, dig_H3, dig_H4, dig_H5, dig_H6
"""
        return self.dig_H1, self.dig_H2, self.dig_H3, \
            self.dig_H4, self.dig_H5, self.dig_H6

    @property
    def sealevel(self):
        return self.__sealevel

    @sealevel.setter
    def sealevel(self, value):
        if 30000 < value < 120000:  # just ensure some reasonable value
            self.__sealevel = value

    @property
    def altitude(self):
        '''
        return altitude in m.
        '''
        try:
            altitude = 44330 * (1.0 - ((self.read_compensated_data()[1] / 256) /
                                self.__sealevel)**0.1903)
        except ValueError:
            altitude = 0.0
        return altitude

    @staticmethod
    def pressure_to_altitude(pressure, sealevel=101325):
        '''
        pressure in Pascal
        return altitude in m.
        '''
        return 44330*(1.0-(pressure/sealevel)**0.1903)

    @staticmethod
    def altitude_to_pressure(altitude, sealevel=101325):
        '''
        Altitude in m.
        return pressure in Pascal
        '''
        if altitude < 44330:
            return sealevel*(1-altitude/44330)**5.255
        return 0.0

    @property
    def dew_point(self):
        """
        Compute the dew point temperature for the current Temperature
        and Humidity measured pair
        """
        from math import log
        t, p, h = self.read_compensated_data()
        t /= 100
        h /= 1024
        h = (log(h, 10) - 2) / 0.4343 + (17.62 * t) / (243.12 + t)
        return (243.12 * h / (17.62 - h)) * 100

    def values(self, compensated_data=None):
        """ human readable values """

        if compensated_data:
            t, p, h = compensated_data
        else:
            # compensated_data is None
            t, p, h = self.read_compensated_data()
        p /= 256
        h /= 1024
        return ("{:.02f} C".format(t/100), "{:.03f} hPa".format(p/100),
                "{:.02f} %".format(h))
