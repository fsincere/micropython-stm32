# (C) Fabrice Sincère
# MicroPython v1.17; NUCLEO-L476RG with STM32L476RG
# test : OK

from pyb import Pin, delay
import onewire
import ds18x20  # module externe à flasher
import micropython

__version__ = (0, 0, 1)

micropython.alloc_emergency_exception_buf(100)

print('''Base shield grove
One Wire Temperature Sensor DS18B20 grove à brancher sur D2
(alimentation en 3.3 V ou 5 V)''')

# capteur de température DS18B20
D2 = Pin('D2')  # par exemple
ds = ds18x20.DS18X20(onewire.OneWire(D2))

# scan for device sensor on the bus one wire
roms = ds.scan()
print('Found device :', roms)
if roms == []:
    raise ValueError("No device found")
rom = roms[0]  # a priori 1 seul capteur

# set resolution
# biblio : https://forum.micropython.org/viewtopic.php?f=16&t=5139
resolution = int(input('Résolution (9 à 12 bits) ? '))  # 9 à 12 bits

if resolution == 9:
    config = b'\x00\x00\x1f'
elif resolution == 10:
    config = b'\x00\x00\x3f'
elif resolution == 11:
    config = b'\x00\x00\x5f'
elif resolution == 12:
    config = b'\x00\x00\x7f'
else:
    raise ValueError("Résolution invalide")
ds.write_scratch(rom, config)

# tc temps de conversion (maximal) :
# 93,75 ms (9 bits 0.5 °C), 750 ms (12 bits 0.0625 °C)
tc = 750/(2**(12-resolution))  # en ms

for i in range(10):
    # lance une nouvelle conversion
    ds.convert_temp()
    # on attend la fin de conversion
    delay(int(tc))
    # lecture de la température
    temperature = ds.read_temp(rom)
    # affichage
    print("{} °C".format(temperature))

print("bye !")
