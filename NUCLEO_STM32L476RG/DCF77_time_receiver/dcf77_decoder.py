# MicroPython v1.19.1 on 2023-02-11; NUCLEO-L476RG with STM32L476RG
# DCF77 module receives DCF77 signal, see https://en.wikipedia.org/wiki/DCF77

# source :
# https://github.com/demogorgi/set-rtc-using-dcf77-via-dcf1

# adaptation : Fabrice Sincère

from pyb import Pin, LED, RTC
from time import sleep, sleep_ms, ticks_ms, ticks_diff

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

print("""
DCF77 Receiver 1060N-03A
module récepteur 4 broches :
VDD : 3.3 V
GND
PON (input) : GND (enable)
OUT (output data) : to pin D8 NUCLEO-L476RG
""")


def detectNewMinute(dcfpin):
    """returns True if the received signal indicates that a new minute begins"""

    print("\nin detectNewMinute...")
    countZeros = 0
    t = 0
    start = ticks_ms()
    while True:
        v = dcfpin.value()
        led.on() if v == 1 else led.off()
        delta = t - ticks_diff(ticks_ms(), start)
        if v == 0:
            countZeros += 1
        else:
            countZeros = 0
        sleep_ms(100 + delta)
        t += 100
        if countZeros == 15:  # 15*100 ms
            # Sekunde 59
            return True


def weekday(i):
    """returns weekday that corresponds to i or 'Invalid day of week'"""
    switcher = {
        1: 'Monday',
        2: 'Tuesday',
        3: 'Wednesday',
        4: 'Thursday',
        5: 'Friday',
        6: 'Saturday',
        7: 'Sunday',
        0: 'Sunday'}
    return switcher.get(i, "Invalid day of week")


def computeTime(dcfpin):
    """decodes the received signal"""
    print("in computeTime...")
    minute, stunde, tag, wochentag, monat, jahr = -1, -1, -1, -1, -1, -1
    a = [0]*7
    secs, bitNum, cnt = 0, 0, 0
    timeInfo = []
    start = ticks_ms()
    # print("bitNum: value")
    while True:
        delta = cnt*50 - ticks_diff(ticks_ms(), start)
        a.pop(0)
        v = dcfpin.value()
        a.append(v)
        led.on() if v == 1 else led.off()

        if a == [0, 1, 1, 1, 1, 1, 0] or a == [0, 1, 1, 1, 1, 0, 0]:
            timeInfo.append(1)
            print("{}: 1".format(bitNum), end='\r')
            bitNum += 1
            secs += 1

        elif a == [0, 1, 1, 0, 0, 0, 0] or a == [0, 1, 1, 1, 0, 0, 0]:
            timeInfo.append(0)
            print("{}: 0".format(bitNum), end='\r')
            bitNum += 1
            secs += 1

        if bitNum == 59:
            if (timeInfo[0] != 0
                or timeInfo[20] != 1
                or sum(timeInfo[21:29]) % 2 != 0
                or sum(timeInfo[29:36]) % 2 != 0
                or sum(timeInfo[36:59]) % 2 != 0):

                print("severe error: parity bits or constant bits \
have unexpected values")
                return True

            minute = (timeInfo[21]
                      + 2*timeInfo[22] + 4*timeInfo[23] + 8*timeInfo[24]
                      + 10*timeInfo[25] + 20*timeInfo[26]
                      + 40*timeInfo[27])

            stunde = (timeInfo[29] + 2*timeInfo[30] + 4*timeInfo[31]
                      + 8*timeInfo[32] + 10*timeInfo[33] + 20*timeInfo[34])
            tag = (timeInfo[36] + 2*timeInfo[37] + 4*timeInfo[38]
                   + 8*timeInfo[39] + 10*timeInfo[40] + 20*timeInfo[41])
            wochentag = timeInfo[42] + 2*timeInfo[43] + 4*timeInfo[44]
            monat = (timeInfo[45] + 2*timeInfo[46] + 4*timeInfo[47]
                     + 8*timeInfo[48] + 10*timeInfo[49])
            jahr = (timeInfo[50] + 2*timeInfo[51] + 4*timeInfo[52]
                    + 8*timeInfo[53] + 10*timeInfo[54] + 20*timeInfo[55]
                    + 40*timeInfo[56] + 80*timeInfo[57])
            print("DCF77 : {:d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d} ({:s})"
                  .format(2000+jahr, monat, tag, stunde, minute, secs,
                          weekday(wochentag)))
            print_RTC_time()  # pour comparaison
            return False
        sleep_ms(50 + delta)
        cnt += 1


def print_RTC_time():
    """print RTC time"""
    # tuple
    year, month, day, day_nb, hour, minute, second, subseconds = rtc.datetime()
    # format
    # (2023, 1, 29, 7, 10, 28, 33, 154)
    # 2023-01-29 Sunday 10:28:33
    print("RTC   : {:04d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d} ({})"
          .format(year, month, day, hour, minute, second,
                  weekday((day_nb+1) % 7)))


# to wake up dcf (maybe connecting to PON pin is sufficient)
# pon_pin = Pin("D7", Pin.OUT)
# pon_pin.on()  # disable
# sleep_ms(200)
# pon_pin.off()  # enable

# led built-in
led = LED(1)

# dcf77 module output
dcf = Pin("D8", mode=Pin.IN)

# avec Thonny, l'horloge temps réel est synchronisée avec l'horloge locale :)
rtc = RTC()
print_RTC_time()

while True:
    if detectNewMinute(dcf):
        print_RTC_time()
        computeTime(dcf)


"""
Le module DCF77 donne l'heure + 1 minute (au prochain top il sera...)
"""

""" exemple :
in detectNewMinute...
RTC   : 2024-03-04 19:18:59
in computeTime...
DCF77 : 2024-03-04 19:20:59 (Monday)
RTC   : 2024-03-04 19:19:58 (Monday)

in detectNewMinute...
RTC   : 2024-03-04 19:19:59
in computeTime...
DCF77 : 2024-03-04 19:21:59 (Monday)
RTC   : 2024-03-04 19:20:58 (Monday)
"""
