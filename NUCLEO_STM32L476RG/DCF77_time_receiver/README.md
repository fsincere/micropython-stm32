## Carte NUCLEO STM32L476RG : module DCF77

4 fils à brancher entre la carte NUCLEO et le module DCF77 :

- GND
- Alimentation Vdd 3.3 V du module DCF77
- Broche OUT du module DCF77 à relier à la broche D8 de la carte NUCLEO-L476RG
- Broche PON à relier à la masse GND


![DCF77 Receiver 1060N-03A pins](images/DCF77 Receiver 1060N-03A_pins.jpg)

La bobine (antenne de réception accordée à la fréquence 77,5 kHz) doit être orientée correctement :

- horizontalement
- axe perpendiculaire à la direction de l'antenne d'émission située à Mainflingen (Allemagne)

Attention aux interférences : certains moniteurs de PC empêchent le bon fonctionnement.  